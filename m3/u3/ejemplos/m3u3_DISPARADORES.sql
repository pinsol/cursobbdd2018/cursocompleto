﻿USE tema3;

CREATE OR REPLACE TABLE entradas(
  id int AUTO_INCREMENT PRIMARY KEY,
  valor int DEFAULT 0,
  total int DEFAULT 0,
  codigo int DEFAULT 0
);

INSERT INTO entradas (valor, codigo)
  VALUES (1,5);
SELECT * FROM entradas;

INSERT INTO entradas (valor, codigo)
  VALUES (1,50);
SELECT * FROM entradas;


DELIMITER //
CREATE OR REPLACE TRIGGER trigger1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
BEGIN
  -- quiero el total como valor al cuadrado
  SET -- dar valor
    new.total=  -- new.campo a reemplazar es necesario
      POW(new.valor,2);  -- funcion potencia, new.campo usado tb es necesario
END //
DELIMITER ;

INSERT INTO entradas (valor, codigo)
  VALUES (1,50);
INSERT INTO entradas (valor, codigo)
  VALUES (2,50);
INSERT INTO entradas (valor, codigo)
  VALUES (3,50);
SELECT * FROM entradas;


DELIMITER //
CREATE OR REPLACE TRIGGER trigger1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
BEGIN
  -- si el codigo es menor que 50 el total es el valor cuadrado y si el codigo es mayor que 50 el total es valor al cubo
DECLARE resultado int;

  IF (codigo<50) THEN
    SET resultado=POW(new.valor,2);
  ELSE
    SET resultado=POW(new.valor,3);
  END IF;

set new.total=resultado;
END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE TRIGGER t3_1
BEFORE UPDATE 
  ON clientes
  FOR EACH ROW
BEGIN
  -- si el codigo postal no tiene 5 digitos saltar un error

  IF (CHAR_LENGTH(new.CP)<>5) THEN
  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'codigo postal debe ser de 5 digitos';
  END IF;
END //
DELIMITER ;




CREATE OR REPLACE TABLE salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int DEFAULT 0,
  fecha date DEFAULT 0,
  edad int DEFAULT 0
);

CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int DEFAULT 0,
  numero int DEFAULT 0,
  codigo int DEFAULT 0
);


-- disparador para la tabla SALAS ANTES de INSERTAR
DELIMITER //
CREATE OR REPLACE TRIGGER salasT1i
BEFORE INSERT
  ON salas
  FOR EACH ROW
BEGIN
  -- disparador que calcula la edad de la sala en funcion de su fecha de alta
  SET new.edad=TIMESTAMPDIFF(year,new.fecha,now());
  
END //
DELIMITER ;

-- para probar el disparador
INSERT INTO salas (butacas, fecha)
  VALUES (150, '2000-1-1');
SELECT * FROM salas;


-- disparador para la tabla SALAS ANTES de ACTUALIZAR
DELIMITER //
CREATE OR REPLACE TRIGGER salasT1u
BEFORE UPDATE
    ON salas
    FOR EACH ROW
BEGIN
  -- disparador que calcula la edad de la sala en funcion de su fecha de alta
  SET new.edad=TIMESTAMPDIFF(year,new.fecha,now());

  IF (CHAR_LENGTH(new.CP)<>5) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'codigo postal debe ser de 5 digitos';
  END IF;
END //
DELIMITER ;

-- para probar el disparador
UPDATE SET (DEFAULT, 150, '2000-1-1');

DELIMITER //
CREATE OR REPLACE TRIGGER ventasT1i
BEFORE INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  -- comprobar que la sala existe
  DECLARE numero int DEFAULT 0;

  SELECT COUNT(*) INTO numero FROM salas WHERE id=new.sala;

  IF (numero=0) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la sala no existe';
  END IF; 
  
END //
DELIMITER ;