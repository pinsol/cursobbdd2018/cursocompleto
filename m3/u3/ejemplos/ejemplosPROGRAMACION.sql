﻿/* Procedimiento que permite indicar cuantos numeros pares hay dentro de la tabla */
USE procedimientosfunciones1;
CREATE OR REPLACE TABLE numeros(
  valor int);
INSERT INTO numeros VALUES (1),(100),(220),(400),(1000);
SELECT * FROM numeros;

DELIMITER //
CREATE OR REPLACE PROCEDURE paresMOD()
BEGIN
  SELECT COUNT(*) FROM numeros WHERE MOD(valor,2)=0;
END //
DELIMITER ;

CALL paresMOD();

    -- prueba funcion MOD
    SELECT MOD(10,4);
    SELECT * FROM numeros WHERE MOD(valor,2)=0; -- xa saber q es un num par el resto de la division de 2 tiene q ser 0


-- CON CURSOR
DELIMITER //
CREATE OR REPLACE PROCEDURE paresCURSOR()
BEGIN
  -- esta variable me ayuda para saber si estoy al final de la tabla cuando leo con el cursor
  DECLARE final int DEFAULT 0;

  -- variable para utilizar con el cursor 
  DECLARE valorLeido int;

  -- variable nos coloca 
  DECLARE contadorPares int DEFAULT 0;

  -- declaro cursor
  DECLARE c CURSOR FOR
  SELECT * 
  FROM numeros;
  
  -- control de excepciones (manejar la excepcion de final de tabla
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET final=1;

  -- iniciamos el cursor
  OPEN c;
    FETCH c INTO valorLeido; -- leemos antes de preguntar
    WHILE (final=0) DO -- la pregunta siempre lo metemos entre parentesis (organizacion de sintaxis)
  
        IF (MOD(valorLeido,2)=0) THEN
          SET contadorPares=contadorPares+1;
        END IF;

    FETCH c INTO valorLeido; -- leemos tambien al final
    END WHILE;  -- final de bucle

  CLOSE c;  -- cerrar el cursor (si no se cierra la tabla se puede quedar solo en modo lectura)
  
  SELECT contadorPares;

END //
DELIMITER ;

CALL paresCURSOR();


/* Encontrar en la tabla nbumeros los registros que san mayores que 5 y almacenar en una tabla nueva (con cursor) */
DELIMITER //
CREATE OR REPLACE PROCEDURE mayoreQ5CURSOR()
BEGIN
  -- esta variable me ayuda para saber si estoy al final de la tabla cuando leo con el cursor
  DECLARE final int DEFAULT 0;

  -- variable para utilizar con el cursor 
  DECLARE valorLeido int;

  -- declaro cursor
  DECLARE c CURSOR FOR
  SELECT valor 
  FROM numeros;
  
  -- control de excepciones (manejar la excepcion de final de tabla
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET final=1;

  -- creamos tabla (previamente borra la tabla si existe)
  DROP TABLE IF EXISTS mayoreQ5;

  CREATE TEMPORARY TABLE mayoreQ5(
  id int AUTO_INCREMENT PRIMARY KEY,
  mayoQ5 int
  );

  -- iniciamos el cursor
  OPEN c;
    FETCH c INTO valorLeido; -- leemos antes de preguntar
    WHILE (final=0) DO -- la pregunta siempre lo metemos entre parentesis (organizacion de sintaxis)
  
        IF (valorLeido>5) THEN
          INSERT INTO mayoreQ5(mayoQ5) VALUES (valorLeido);
        END IF;

    FETCH c INTO valorLeido; -- leemos tambien al final
    END WHILE;  -- final de bucle

  CLOSE c;  -- cerrar el cursor (si no se cierra la tabla se puede quedar solo en modo lectura)
  
SELECT mayoQ5 FROM mayoreQ5;

END //
DELIMITER ;

CALL mayoreQ5CURSOR();


/* Completar la tabla:
  repeticiones: num de veces que se repite la nota hasta el valor leido
  valor: la nota pero con texto
    0 - 5: Suspenso
    5 - 6: Suficiente
    6 - 7: Bien
    8 - 9: Notable
    9 - 10: Sobresaliente */

CREATE OR REPLACE TABLE notas(
  id int AUTO_INCREMENT,
  nota int,
  repeticiones int,
  valor varchar(50),
  PRIMARY KEY(id)
);


INSERT INTO notas(nota)
  VALUES(5),(5),(3),(7),(9),(9),(10),(5),(1);


DELIMITER //
CREATE OR REPLACE PROCEDURE completarTabla()
BEGIN
  -- esta variable me ayuda para saber si estoy al final de la tabla cuando leo con el cursor
  DECLARE final int DEFAULT 0;

  -- variable para utilizar con el cursor 
  DECLARE notaLeida int;
  DECLARE textoNota varchar(20);
  DECLARE r0 int DEFAULT 0;
  DECLARE r1 int DEFAULT 0;
  DECLARE r2 int DEFAULT 0;
  DECLARE r3 int DEFAULT 0;
  DECLARE r4 int DEFAULT 0;
  DECLARE r5 int DEFAULT 0;
  DECLARE r6 int DEFAULT 0;
  DECLARE r7 int DEFAULT 0;
  DECLARE r8 int DEFAULT 0;
  DECLARE r9 int DEFAULT 0;
  DECLARE repeticiones int DEFAULT 0;
  DECLARE registro int;

  -- declaro cursor
  DECLARE c CURSOR FOR
  SELECT nota 
  FROM notas;
  
  -- control de excepciones (manejar la excepcion de final de tabla
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET final=1;


  -- iniciamos el cursor
  OPEN c;
    FETCH c INTO registro, notaLeida; -- leemos antes de preguntar Y EL REGISTRO!!!!

    WHILE (final=0) DO -- la pregunta siempre lo metemos entre parentesis (organizacion de sintaxis)
       -- calculo el texto de la nota
         SET textoNota=notaTexto(notaLeida);

       -- calculo las repetciones
          CASE notaLeida
            WHEN 0 THEN
               SET repeticiones=r0;
               SET r0=r0+1;
            
            WHEN 1 THEN
               SET repeticiones=r1;
               SET r1=r1+1;

            WHEN 2 THEN
               SET repeticiones=r2;
               SET r2=r2+1;

            WHEN 3 THEN
               SET repeticiones=r3;
               SET r3=r3+1;

            WHEN 4 THEN
               SET repeticiones=r4;
               SET r4=r4+1;

            WHEN 5 THEN
               SET repeticiones=r5;
               SET r5=r5+1;

            WHEN 6 THEN
               SET repeticiones=r6;
               SET r6=r6+1;

             WHEN 7 THEN
               SET repeticiones=r7;
               SET r7=r7+1;

            WHEN 8 THEN
               SET repeticiones=r8;
               SET r8=r8+1;

            ELSE
               SET repeticiones=r9;
               SET r9=r9+1;
          END CASE;


   UPDATE notas
    SET valor=textoNota,
        repeticiones=repeticiones
    WHERE
        id=registro;  -- CUIDADO!! el importantisimo WHERE!! ;)

    FETCH c INTO registro, notaLeida; -- leemos la nota siguiente IMPORTANTE EL REGISTRO!!!
    END WHILE;  -- final de bucle

  CLOSE c;  -- cerrar el cursor (si no se cierra la tabla se puede quedar solo en modo lectura)
  

END //
DELIMITER ;

CALL completarTabla();


DELIMITER //
CREATE OR REPLACE FUNCTION notaTexto(valor INT)
RETURNS varchar(50)
BEGIN
  DECLARE resultado varchar(50);
  CASE
          WHEN (valor<5) THEN SET resultado='Suspenso';
          WHEN (valor<6) THEN SET resultado='Suficiente';
          WHEN (valor<7) THEN SET resultado='Bien';
          WHEN (valor<9) THEN SET resultado='Notable';
          ELSE SET  resultado='Sobresaliente';
  END CASE;
  RETURN resultado;
END //
DELIMITER ;

SELECT notaTexto(3);



DELIMITER
CREATE OR REPLACE PROCEDURE nombre(atr)
BEGIN
  /* varibles */
  DECLARE var ;

  /* cursores */
  DECLARE c CURSOR FOR
    SELECT * FROM tabla;

  /* excepciones */
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET atr=valor

  /* programa */

END //
DELIMITER ; 