﻿                                       /* Disparadores - EJEMPLO 7 */
USE ejemplo7;

SELECT * FROM clientes;
SELECT * FROM errores;
SELECT * FROM localidades;
SELECT * FROM provincias;


/* 1.- Crear un disparador para que cuando inserte un registro en clientes me compruebe
       si la edad esta entre 18 y 70. En caso de que no esté produzca una excepción con
       el mensaje “La edad no es válida” */

ALTER TABLE clientes ADD COLUMN edad int;  -- creamos campo edad que no tiene la tabla

-- Disparador de la tabla CLIENTES ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER clientes_bi
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  IF new.edad<18 OR new.edad>70 THEN
  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es válida';
  END IF;
END //
DELIMITER ;

  -- para probar el disparador
INSERT clientes (edad) VALUES (10);
SELECT * FROM clientes;


-- con RESIGNAL
  DELIMITER //
  CREATE OR REPLACE TRIGGER clientes_bi
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
      BEGIN
        RESIGNAL SET MESSAGE_TEXT='La edad no es válida';
      END;
    IF (new.edad NOT BETWEEN 18 AND 70) THEN
      SIGNAL SQLSTATE '45000';
    END IF;
  END //
  DELIMITER ;

/* 2.- Además, cada vez que un usuario introduzca un registro con una edad no valida
       debe grabar ese registro en una tabla denominada errores */

-- Disparador de la tabla CLIENTES ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER clientes_bi
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  IF new.edad<18 OR new.edad>70 THEN
    INSERT INTO errores (mensaje, fecha, hora)
     VALUES (CONCAT(new.edad,' ','error 45000 - La edad no es válida'),CURDATE(),CURTIME());
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es válida';
    
  END IF;

  
END //
DELIMITER ;

  -- para probar el disparador
INSERT clientes (edad) VALUES (10);
SELECT * FROM errores;


/* 3.- Crear un disparador para que cuando modifique un registro de la tabla clientes
       me compruebe si la edad esta entre 18 y 70. En caso de que no esté produzca
       una excepción con el mensaje “La edad no es válida” */

-- Disparador de la tabla CLIENTES ANTES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER clientes_bu
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  IF new.edad<18 OR new.edad>70 THEN
  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es válida';
  END IF;
END //
DELIMITER ;

  -- para probar el disparador
UPDATE clientes SET edad=10;
SELECT * FROM clientes;

/* 4.- Crear disparadores en la tabla localidades que comprueben que
       cuando introduzcas o modifiques una localidad el código de la provincia
       exista en la tabla provincias. En caso de que no exista que cree la provincia.
       El resto de campos de la tabla provincias se colocarán a valor por defecto */

-- Disparador de la tabla LOCALIDADES ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER localidades_bi
BEFORE INSERT
  ON localidades
  FOR EACH ROW
BEGIN
  DECLARE numero int;
  SELECT COUNT(*) INTO numero FROM provincias p WHERE p.CodPro=new.CodPro;
  IF (numero=0) THEN
    INSERT INTO provincias (CodPro)
      VALUES (new.CodPro);
  END IF;

  /* otra opcion:

  IF new.CodPro NOT IN (SELECT CodPro FROM provincias) THEN
    INSERT INTO provincias VALUES (new.CodPro,DEFAULT);
  END IF;

  */

END //
DELIMITER ;

  -- para probar el disparador
INSERT localidades (CodPro) VALUES (06362);
SELECT * FROM provincias;

-- Disparador de la tabla LOCALIDADES ANTES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER localidades_bu
BEFORE UPDATE
  ON localidades
  FOR EACH ROW
BEGIN
  DECLARE numero int DEFAULT 0;

  IF new.edad<18 OR new.edad>70 THEN
  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es válida';
  END IF;

  SELECT COUNT(*) INTO numero FROM localidades l WHERE l.CodLoc=new.CodLoc;
  IF (numero=0) THEN
    SIGNAL SQLSTATE '45000' set MESSAGE_TEXT='La localidad no existe';
  END IF;
END //
DELIMITER ;

  -- para probar el disparador
UPDATE localidades SET CodPro=06362;
SELECT * FROM provincias;

/* 5.- Modificar los disparadores de la tabla clientes para que
       cuando introduzcas o modifiques un cliente compruebe si la localidad del cliente exista.
       En caso que no exista mostrar el mensaje de error “La localidad no existe”.
       Debe insertar ese registro de error en la tabla errores */

-- Disparador de la tabla CLIENTES ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER clientes_bi
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  DECLARE numero int DEFAULT 0;
  SELECT COUNT(*) INTO numero FROM provincias p WHERE p.CodPro=new.CodPro;
  IF (numero=0) THEN
    INSERT INTO provincias (CodPro)
      VALUES (new.CodPro);
  END IF;
END //
DELIMITER ;

  -- para probar el disparador
INSERT localidades (NombLoc) VALUES ('Santander');
SELECT * FROM clientes;

-- Disparador de la tabla CLIENTES ANTES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER clientes_bu
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  IF new.NombLoc IN (SELECT NombLoc FROM localidades) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La localidad no existe';
  END IF;
END //
DELIMITER ;

  -- para probar el disparador
UPDATE localidades SET NombLoc='Santander';
SELECT * FROM clientes;


/* 6.- Crear unos disparadores para la tabla provincias que
       cuando modifiques o insertes un registro
       te realice algunas operaciones
        - Te calcule las iniciales de la provincia introducida
        - Te compruebe si la provincia por error se haya utilizado ya en localidades.
          En caso de que si debe calcular el número de localidades que tiene la provincia
*/

ALTER TABLE provincias ADD COLUMN Iniciales varchar(1);  -- creamos campo edad que no tiene la tabla
ALTER TABLE provincias ADD COLUMN Cantidad varchar(1);  -- creamos campo edad que no tiene la tabla

-- Disparador de la tabla PROVINCIAS ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER provincias_bi
BEFORE INSERT
  ON provincias
  FOR EACH ROW
BEGIN
  DECLARE numero int DEFAULT 0;

  SET new.Iniciales=LEFT(new.NombPro,1);

  SELECT COUNT(*) INTO numero FROM localidades l WHERE l.CodPro=new.CodPro;
  SET new.Cantidad=numero; 

END //
DELIMITER ;


/* 7.- Modificar los disparadores para la tabla localidades para que cuando insertes o
       modifiques un registro te recalcule el campo cantidad de la tabla provincias */

DELIMITER //
CREATE OR REPLACE TRIGGER localidades_ai
AFTER INSERT
  ON localidades
  FOR EACH ROW
BEGIN
  DECLARE numero int;
  UPDATE provincias p
    SET p.Cantidad=(
        SELECT COUNT(*) INTO numero FROM localidades l
          WHERE l.CodPro=new.CodPro
        )
    WHERE p.CodPro=new.CodPro;
END //
DELIMITER ;


-- Disparador de la tabla LOCALIDADES ANTES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER localidades_au
AFTER UPDATE
  ON localidades
  FOR EACH ROW
BEGIN

END //
DELIMITER ;

  -- para probar el disparador
UPDATE localidades SET NombLoc='';
SELECT * FROM localidades;

