﻿CREATE DATABASE IF NOT EXISTS restricciones;
USE restricciones;

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  edad int,
  PRIMARY KEY(id)
  );

ALTER TABLE clientes ADD COLUMN edad1 int;

CREATE OR REPLACE VIEW vista1 AS
  SELECT * FROM clientes
  WHERE edad>=0
WITH LOCAL CHECK OPTION
;

TRUNCATE clientes;

INSERT IGNORE INTO vista1 VALUES
  (DEFAULT, -10),
  (DEFAULT, 10);


SELECT * FROM vista1;
SELECT * FROM clientes c;


CREATE OR REPLACE VIEW vista2 AS
  SELECT * FROM clientes
  WHERE edad>=0 AND edad1>edad
WITH LOCAL CHECK OPTION
;

TRUNCATE clientes;

INSERT IGNORE INTO vista2 VALUES
  (DEFAULT, -10,10), -- la edad no cumple
  (DEFAULT, 10,0),  -- la edad1 es menor que edad
  (DEFAULT, 10,20);  -- correcto


SELECT * FROM vista2;
SELECT * FROM clientes c;





CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(id)
  );


ALTER TABLE ventas
  ADD CONSTRAINT FKVentasClientes FOREIGN KEY (cliente)
  REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;
  

  -- vista3
-- fecha de ventas menor que hoy
-- cantidad mayor que 10 y menor que 100

CREATE OR REPLACE VIEW vista3 AS
  SELECT * FROM ventas
  WHERE fecha<=NOW() AND cantidad BETWEEN 10 AND 100;

INSERT IGNORE INTO vista3 VALUES
  (DEFAULT, NULL, '2020/1/1', 0),
  (DEFAULT, NULL, '2020/1/1', 80),
  (DEFAULT, NULL, '2018/1/1', 80);

SELECT * FROM vista3;
SELECT * FROM ventas;

TRUNCATE ventas;

CREATE OR REPLACE VIEW vista3 AS
  SELECT * FROM ventas
  WHERE fecha<=NOW() AND cantidad BETWEEN 10 AND 100
WITH LOCAL CHECK OPTION
;

-- vista4
-- cantidad mayor que 10 y menor que 100

CREATE OR REPLACE VIEW vista4 AS
  SELECT * FROM ventas
  WHERE cantidad BETWEEN 10 AND 100;

-- vista5
-- con las dos restricciones

  CREATE OR REPLACE VIEW vista5 AS
  SELECT * FROM vista4
  WHERE fecha<NOW();


  CREATE OR REPLACE VIEW vista4 AS
  SELECT * FROM ventas
  WHERE cantidad BETWEEN 10 AND 100
  WITH LOCAL CHECK OPTION;

  CREATE OR REPLACE VIEW vista5 AS
  SELECT * FROM vista4
  WHERE fecha<NOW()
  WITH LOCAL CHECK OPTION;

  TRUNCATE ventas;

INSERT IGNORE INTO vista4 VALUES
  (DEFAULT, NULL, '2020/1/1', 0), -- no
  (DEFAULT, NULL, '2020/1/1', 80),  -- si
  (DEFAULT, NULL, '2018/1/1', 80);  -- si


TRUNCATE ventas;

INSERT IGNORE INTO vista5 VALUES
  (DEFAULT, NULL, '2020/1/1', 0),  -- no
  (DEFAULT, NULL, '2020/1/1', 80), -- no
  (DEFAULT, NULL, '2018/1/1', 80),  -- si
  (DEFAULT, NULL, '2018/1/1', 0);  -- no (pero si lo hace por el local)

SELECT * FROM ventas;

-- vista 6
  -- with cascaded check option

  TRUNCATE ventas;

   CREATE OR REPLACE VIEW vista6 AS
  SELECT * FROM vista4
  WHERE fecha<NOW()
  WITH cascaded CHECK OPTION;

INSERT IGNORE INTO vista6 VALUES
  (DEFAULT, NULL, '2020/1/1', 0),  -- no
  (DEFAULT, NULL, '2020/1/1', 80), -- no
  (DEFAULT, NULL, '2018/1/1', 80), -- si
  (DEFAULT, NULL, '2018/1/1', 0);  -- no

SELECT * FROM ventas;

-- campo nuevo ventas en cliente cuantas ventas tiene ese cliente

  ALTER TABLE clientes
    ADD COLUMN venta int;

  SELECT * FROM clientes;

  UPDATE clientes JOIN (SELECT COUNT(*) total FROM ventas GROUP BY cliente) c1 ON clientes.id=c1.total
    SET cliente.venta=c1.total;

INSERT IGNORE INTO vista6 VALUES
  (DEFAULT, 2, '2018/1/1',80),
  (DEFAULT, 2, '2018/1/1',80);