﻿--  EJEMPLOS CON IF


  -- ejemplo de eleccion simple

/*
  ej35
    OBJETIVO: Mostrar las palabras GRANDE o pequeño si el numero introducido es mayor o menor de 10

    ARGUMENTOS: numero solicitado
  */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej35;

  CREATE PROCEDURE ej35(numero int)
  BEGIN
    -- SELECT if(numero>10,'GRANDE','pequeño');
    IF (numero>10) THEN SELECT 'GRANDE';
      ELSE SELECT 'pequeño';
    END IF;
  END //
    DELIMITER;

  -- probando el procedimiento
  CALL ej35(12);
  CALL ej35(5);

  -- ejemplo temporary table (tablas temporales)

/* ej4
      OBJETIVO: tabla temporal

      ARGUMENTOS: 2 numeros
  */
  
  DELIMITER //
  DROP PROCEDURE IF EXISTS ej4 //

  CREATE PROCEDURE ej4(n1 int, n2 int)
  BEGIN
      DROP TABLE IF EXISTS ej4;
      CREATE TEMPORARY TABLE ej4(
        indice int AUTO_INCREMENT,
        valor int,
        PRIMARY KEY(indice)
        );

   -- SET ej4=n1;
      INSERT INTO ej4(valor) VALUES
        (n1),
        (n2);

      SELECT valor FROM ej4 ORDER BY valor DESC; -- ordenar los valores añadidos en orden de mayor a menor
  END //
    DELIMITER;

  -- probando el procedimiento
  CALL ej4(12,34);


    -- 

/* ej5
    OBJETIVO:
    ARGUMENTOS:
  */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej5 //

  CREATE PROCEDURE ej5(arg1 int,OUT arg2 int,INOUT arg3 int)
  BEGIN
      SELECT arg1, arg2, arg3;      
  END //
    DELIMITER;

SET @numero3=10
CALL ej5(1,@numero2,@numero3);



/*
  ej6

*/

