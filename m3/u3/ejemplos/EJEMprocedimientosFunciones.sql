﻿/*
  Ejemplos de procedimientos y funciones
*/

CREATE DATABASE procedimientosFunciones1;
USE procedimientosFunciones1;

/*
  crear un procedimiento almacenado
  muestre la fecha de hoy
*/

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej1 //
  CREATE PROCEDURE ej1()
  BEGIN
      SELECT NOW();
  END //
  DELIMITER;

/*
  llamar al procedimiento almacenado
*/

CALL ej1();

/*
  crear una variable de tipo datetime y almacenar en esa variable la fecha de hoy
*/

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej2 //
  CREATE PROCEDURE ej2()
  BEGIN
      DECLARE v1 datetime; -- declarando
      SET v1=NOW(); -- asignando
      SELECT v1;
  END //
  DELIMITER;

/* llamo al procedimiento almacenado */
  CALL ej2();

/* Suma de dos numeros */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej3 //
  CREATE PROCEDURE ej3(arg1 int, arg2 int)
  BEGIN
      DECLARE suma int;
      SET suma=arg1+arg2;
      SELECT suma;
  END //
  DELIMITER;

/* llamar al procedimiento */
CALL ej3(3,4);
CALL ej3(6,4);
CALL ej3(6,5);

/* resta de dos numeros */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej3a //
  CREATE PROCEDURE ej3a(arg1 int, arg2 int)
  BEGIN
      DECLARE resta int;
      SET resta=arg1-arg2;
      SELECT resta;
  END //
  DELIMITER;

/* llamar al procedimiento */
CALL ej3a(3,4);
CALL ej3a(6,4);
CALL ej3a(6,5);


/* suma de dos numeros y almacenamiento en una tabla */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej31 //
  CREATE PROCEDURE ej31(arg1 int, arg2 int)
  BEGIN
    
    -- declarar las variables    
      DECLARE sumatabla int DEFAULT 0;
    -- crear la tabla para almacenar los resultados
      CREATE TABLE IF NOT EXISTS datos(
        id int AUTO_INCREMENT,
        dato1 int,
        dato2 int,
        suma int,
        PRIMARY KEY(id)
        );
    -- calcular la suma
       SET sumatabla=arg1+arg2;
    -- introducir los datos en la tabla
      INSERT INTO datos VALUES
        (DEFAULT,arg1,arg2,sumatabla);

  END //
  DELIMITER;

/* llamar al procedimiento */
CALL ej31(3,4);
CALL ej31(6,4);
CALL ej31(6,5);
CALL ej31(12,5);
CALL ej31(12,15);

SELECT * FROM datos;

/*
  Ej32
  SUMA, producto 2 NUMEROS
  Crear una tabla llamada ej32(id, d1, d2, suma, producto)
  Introducir los datos en la tabla
  */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej32 //
  CREATE PROCEDURE ej32(d1 int, d2 int)
  BEGIN
    
    -- declarar las variables    
      DECLARE sumatabla1 int DEFAULT 0;
      DECLARE producto int DEFAULT 0;
    -- crear la tabla para almacenar los resultados
      CREATE TABLE IF NOT EXISTS ej32(
        id int AUTO_INCREMENT,
        d1 int,
        d2 int,
        suma int,
        producto int,
        PRIMARY KEY(id)
        );
    -- calcular la suma
       SET sumatabla1=d1+d2,
    -- calcular el producto
           producto=d1*d2;
    -- introducir los datos en la tabla
      INSERT INTO ej32 VALUES
        (DEFAULT,d1,d2,sumatabla1,producto);
  END //
    DELIMITER;

/* llamar al procedimiento */
CALL ej32(3,4);
CALL ej32(6,4);
CALL ej32(6,5);
CALL ej32(12,5);
CALL ej32(12,15);

SELECT * FROM ej32;


/*
  Ej33
  OBJETIVO:
  Crear una tabla llamada ej33(id, dato1, dato2, resultado).
  Si existe la vuelve a crear
*/

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej33 //
  CREATE PROCEDURE ej33()
  BEGIN
    -- crear la tabla para almacenar los resultados
      DROP TABLE IF EXISTS ej33; -- si existe la borra y...
      CREATE TABLE ej33(          -- ...la vuelve a crear ;)
        id int AUTO_INCREMENT,
        dato1 int,
        dato2 int,
        resultado int,
        PRIMARY KEY(id)
        );
  END //          -- OJO!! la tabla no se crea, esto es solo un procedimiento, para ello haremos una llamada
    DELIMITER;


    CALL ej33();  -- la llamada crea el procedimiento y ya sale la tabla

SELECT * FROM ej33;

/*
  Ej33a  
  OBJETIVO: potencia de un numero con otro.
  Introducir los datos en la tabla
  
  ARGUMENTOS:
  dato1- base
  dato2- exponente
  resultado- el valor de la potencia (funcion POW)
  */

  
  DELIMITER //
  DROP PROCEDURE IF EXISTS ej33a //
  CREATE PROCEDURE ej33a(dato1 int, dato2 int)
  BEGIN
    
    -- crear las variables    
      DECLARE potencia int DEFAULT 0;
    -- calcular la potencia
       SET potencia=POW(dato1,dato2);
    -- introducir los datos en la tabla
      INSERT INTO ej33 VALUES
        (DEFAULT,dato1,dato2,potencia);
  END //
    DELIMITER;

  -- probando el procedimiento
  CALL ej33a(1,2);

  -- comprobando los resultados mirando la tabla donde se almacenan
  SELECT * FROM ej33;


/*
  Ej33b
  OBJETIVOS:
  Realizar la raiz cuadrada de un numero.
  Introducir los datos en la tabla

  ARGUMENTOS:
  dato1- numero a saber su raiz
  resultado- el valor de la raiz cuadrada (funcion SQR)
  */

  
  DELIMITER //
  DROP PROCEDURE IF EXISTS ej33b //
  CREATE PROCEDURE ej33b(dato1 int)
  BEGIN
    
    -- declarar las variables    
      DECLARE raiz int DEFAULT 0;
    -- calcular la potencia
       SET raiz=SQRT(dato1);
    -- introducir los datos en la tabla
      INSERT INTO ej33 VALUES
        (DEFAULT,dato1,2,raiz);
  END //
    DELIMITER;

  -- probando el procedimiento
  CALL ej33b(9);
  
  -- comprobando los resultados mirando la tabla donde se almacenan
  SELECT * FROM ej33;


/*
  ej34
  CREAR TABLA (id int,texto varchar50,longitud int,caracteres varchar50)
*/  

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej34 //
  CREATE PROCEDURE ej34()
  BEGIN
    -- crear la tabla para almacenar los resultados
      DROP TABLE IF EXISTS ej34;
      CREATE TABLE ej34(
        id int AUTO_INCREMENT,
        texto varchar(50),
        longitud int,
        caracteres varchar(50),
        PRIMARY KEY(id)
        );
  END //
    DELIMITER;


    CALL ej34();

SELECT * FROM ej34;
   
  /* 
    ej34a
    OBJETIVO: actualizar longitud

    ARGUMENTOS: ninguno

    **OBSERVACIONES: al no haber textos, primero ejecutamos ej34b**
  */ 

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej34a //
  CREATE PROCEDURE ej34a()
  BEGIN
    -- calcular la longitud y lo introducimos en la tabla
       UPDATE ej34
       SET longitud=LENGTH(texto);
  END //
    DELIMITER;

  -- probando el procedimiento
  CALL ej34a();
  
  -- comprobando los resultados mirando la tabla donde se almacenan
  SELECT * FROM ej34;


  /* 
    ej34b
    OBJETIVO: actualizar registro

    ARGUMENTOS: texto
  */ 

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej34b //
  CREATE PROCEDURE ej34b(texto varchar(50))
  BEGIN
   -- introducir el texto en su campo TEXTO
      INSERT INTO ej34(texto) VALUE
        (texto);
  END //
    DELIMITER;

    -- haciendo pruebas
        CALL ej34b('ejemplo de clase');
        CALL ej34b('ejemplo');
        CALL ej34b('de clase');

    -- comprobando en la tabla
        SELECT * FROM ej34; 


    /* 
    ej34c
    OBJETIVO: actualizar caracteres

    ARGUMENTOS: insertar registro
  */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej34c //
  CREATE PROCEDURE ej34c(numCaracteres int, registro int)
  BEGIN
    -- refleja los caracteres desde lo indicado de los registros desde lo indicado
       UPDATE ej34 JOIN (SELECT * FROM ej34 LIMIT registro,1000) c1 USING(id)
       SET ej34.caracteres=LEFT(ej34.texto,numCaracteres);
  END //
    DELIMITER;

    -- haciendo pruebas
        CALL ej34c(5,8);

    -- comprobando en la tabla
        SELECT * FROM ej34; 