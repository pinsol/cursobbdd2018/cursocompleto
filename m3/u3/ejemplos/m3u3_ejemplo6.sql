﻿                                       /* Disparadores - EJEMPLO 6 */
USE ejemplo6;

/* 1.- Crear un disparador para la tabla ventas para que cuando metas un registro nuevo
       te calcule el total automáticamente */

-- Disparador de la tabla VENTAS ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_1i
BEFORE INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  -- disparador que calcula el total
  SET new.total=new.unidades*new.precio;
END //
DELIMITER ;

-- para probar el disparador
INSERT ventas (id,producto,precio,unidades) VALUES (DEFAULT,'p1',40,10);
SELECT * FROM ventas;


/* 2.- Crear un disparador para la tabla ventas para que cuando inserte un registro me sume
       el total a la tabla productos (en el campo cantidad) */

-- Disparador de la tabla VENTAS ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_2bi
BEFORE INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  -- disparador que calcula el total
  SET new.total=new.unidades*new.precio;
END //
DELIMITER ;



DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_2ai
AFTER INSERT
  ON ventas
  FOR EACH ROW
BEGIN
  UPDATE productos p
    SET p.cantidad=p.cantidad+(new.total-old.total)
    WHERE p.producto=new.producto;
END //
DELIMITER ;

-- para probar el disparador
INSERT ventas (id,producto,precio,unidades) VALUES (DEFAULT,'p1',20,10);
SELECT * FROM ventas;
SELECT * FROM productos;


/* 3.- Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo
       te calcule el total automáticamente */

-- Disparador de la tabla VENTAS ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_3u
BEFORE UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  -- disparador que calcula el total
  SET new.total=new.unidades*new.precio;
END //
DELIMITER ;

-- para probar el disparador
INSERT ventas (id,producto,precio,unidades) VALUES (DEFAULT,'p6',40,10);
SELECT * FROM ventas;


/* 4.- Crear un disparador para la tabla ventas para que cuando actualice un registro me
        sume el total a la tabla productos (en el campo cantidad) */

-- Disparador de la tabla VENTAS DESPUES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_4u
AFTER UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  -- disparador que actualiza la cantidad
  SET cantidad=cantidad-old.total+new.total;
END //
DELIMITER ;

-- para probar el disparador
INSERT ventas (id,producto,precio,unidades) VALUES (DEFAULT,'p6',40,10);
SELECT * FROM ventas;


/* 5.- Crear un disparador para la tabla productos que si cambia el código del producto te
       sume todos los totales de ese producto de la tabla ventas */

-- Disparador de la tabla PRODUCTOS DESPUES de ACTUALIZAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER productos6_5u
AFTER UPDATE
  ON productos
  FOR EACH ROW
BEGIN
  SET new.cantidad(
    SELECT SUM(v.total) FROM ventas v
    WHERE v.producto=new.producto
    );
END //
DELIMITER ;

/* 6.- Crear un disparador para la tabla productos que si eliminas un producto te elimine
       todos los productos del mismo código en la tabla ventas */

-- Disparador de la tabla PRODUCTOS DESPUES de ELIMINAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER productos6_6d
AFTER DELETE
  ON productos
  FOR EACH ROW
BEGIN
  DELETE FROM ventas WHERE producto=old.producto;
END //
DELIMITER ;

/* 7.- Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del
       campo cantidad de la tabla productos (en el campo cantidad) */

-- Disparador de la tabla VENTAS DESPUES de ELIMINAR un registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_7d
AFTER DELETE 
  ON ventas
  FOR EACH ROW
BEGIN
UPDATE productos
  set 
END //
DELIMITER ;
 

/* 8.- Modificar el disparador 3 para que modifique la tabla productos actualizando
       el valor del campo cantidad en funcion del total */
-- Disparador de la tabla VENTAS ANTES de INSERTAR nuevo registro
DELIMITER //
CREATE OR REPLACE TRIGGER ventas6_3u_b
BEFORE UPDATE
  ON ventas
  FOR EACH ROW
BEGIN
  SET new.total=new.unidades*new.precio;
  -- esto es recomendable realizarlo depues del after
  UPDATE productos
    SET cantidad=cantidad+(new.total-old.total)
    WHERE producto=new.producto
END //
DELIMITER ;

-- para probar el disparador
INSERT ventas (id,producto,precio,unidades) VALUES (DEFAULT,'p6',40,10);
SELECT * FROM ventas;