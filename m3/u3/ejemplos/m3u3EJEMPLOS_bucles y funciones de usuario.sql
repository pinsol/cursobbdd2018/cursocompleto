﻿USE procedimientosfunciones1;
                                                /* Ejemplos de bucles */

-- creo una tabla pruebas

CREATE OR REPLACE TABLE pruebas(
  ID int AUTO_INCREMENT,
  numeros int DEFAULT 0,
  PRIMARY KEY (id)
  );

/**
-- procedimiento almacenado bucle1
-- ARGUMENTOS: A1 entero, A2 entero
-- INTRODUCIR DATOS EN LA TABLA PRUEBAS DESDE EL ARGUMENTO1
-- HASTA EL ARGUMENTO2
-- NO DEBE INTRODUCIR NI EL 8 NI EL 12
**/


  -- version con ITERATE

  DELIMITER //

CREATE OR REPLACE PROCEDURE bucle1(IN a1 int,IN a2 int)
BEGIN 
  DECLARE contador int DEFAULT a1;  -- variable contador
-- comienzo del bucle etiqueta
etiqueta: WHILE(contador<=a2) DO

    IF (contador=8 OR contador=12) THEN
      SET contador=contador+1;
      ITERATE etiqueta;
    END IF;

  INSERT INTO pruebas VALUE(DEFAULT,contador); -- introduzco los datos en la tabla
  SET contador=contador+1; -- actualizo el contador
  END WHILE etiqueta;
-- fin del bucle etiqueta 
   
  END //
  DELIMITER ;

-- para probar el procedimiento
CALL bucle1(5,20);
SELECT * FROM pruebas;



  -- version SIN ITERATE (WHILE)
DELIMITER //

CREATE OR REPLACE PROCEDURE bucle2(IN a1 int,IN a2 int)
BEGIN 
  DECLARE contador int DEFAULT a1;  -- variable contador
  
  -- comienzo del bucle
  WHILE(contador<=a2) DO
    IF (contador<>8 AND contador<>12) THEN
    
      INSERT INTO pruebas VALUE(DEFAULT,contador); -- introduzco los datos en la tabla
    END IF;

  SET contador=contador+1; -- actualizo el contador

END WHILE;
-- fin del bucle etiqueta 
   
  END //
  DELIMITER ;

                                             /* Ejemplos de funciones */

-- creo una tabla pruebas

CREATE OR REPLACE TABLE pruebas(
  ID int AUTO_INCREMENT,
  numeros int DEFAULT 0,
  PRIMARY KEY (id)
  );

/**
-- FUNCION1
-- ARGUMENTOS: N1, N2 y N3
-- CREAR UNA FUNCION QUE LE PASEMOS 3 NUMEROS Y TE
-- DEVUELVA LA SUMA DE ELLOS
-- SI LOS NUMEROS NO SON MAYORES QUE 0 DEVUELVE NULL
**/


DELIMITER //

CREATE OR REPLACE FUNCTION funcion1(n1 int,n2 int,n3 int)
RETURNS int
BEGIN
  DECLARE suma int DEFAULT NULL;

  -- analiza los datos y preparas el valor a devolver
  IF(n1>0 AND n2>0 AND n3>0) THEN
    SET suma=n1+n2+n3;
  END IF;

  -- al final realizo el return
  RETURN suma; 
END //
DELIMITER ;

-- para probar la funcion
SELECT funcion1(23,34,2);