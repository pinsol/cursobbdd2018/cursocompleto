SET NAMES 'utf8';

USE procedimientosfunciones1;

/** 
  MOSTRAR ERRORES
 **/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE errores1()  -- DECLARES CONTINUE: continua el proceso
  BEGIN 

    -- control de excepciones
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION 
      BEGIN
        INSERT INTO ramon1 VALUES(RAND()*1000);
      END;
    -- fin del control de excepciones

    CREATE tABLE ramon1(id int PRIMARY KEY);
    INSERT INTO ramon1 VALUES(1);           -- sale los errores (de dos en dos)
  END //
  DELIMITER ;

CALL errores1();
SELECT * FROM ramon1;

DELIMITER //
  CREATE OR REPLACE PROCEDURE errores2()
  BEGIN 
    DECLARE EXIT HANDLER FOR SQLEXCEPTION   -- DECLARES EXIT: sale del proceso
      BEGIN
        INSERT INTO ramon1 VALUES(RAND()*1000);
      END;
    CREATE tABLE ramon1(id int PRIMARY KEY);
    INSERT INTO ramon1 VALUES(1);            -- sale un error porque se detienen al llegar al error
  END //
  DELIMITER ;

CALL errores2();
SELECT * FROM ramon1;

DELIMITER //
  CREATE OR REPLACE PROCEDURE errores3()
  BEGIN 
    DECLARE CONTINUE HANDLER FOR SQLWARNING   -- FOR SQLWARNING
      BEGIN
        INSERT INTO ramon1 VALUES(RAND()*1000);  -- es una SQL excepcion
      END;
    INSERT INTO ramon1 VALUES(1);             -- salta porque no se trata de una sqlwarning
  END //
  DELIMITER ;

CALL errores3();
SELECT * FROM ramon1;

DELIMITER //
  CREATE OR REPLACE PROCEDURE errores4()
  BEGIN 
    DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' -- con ese codigo sale varios errores, mejor indicar el primer codigo
      BEGIN
        INSERT INTO ramon1 VALUES(RAND()*1000);
      END;
    
    INSERT INTO ramon1 VALUES(1);
  END //
  DELIMITER ;

CALL errores4();
SELECT * FROM ramon1;


CREATE OR REPLACE TABLE ramon2(
  id int AUTO_INCREMENT,
  valor int,
  PRIMARY KEY(id)
  );

DELIMITER //
  CREATE OR REPLACE PROCEDURE errores5()
  BEGIN 
    DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' 
      BEGIN
        INSERT INTO ramon1 VALUES(RAND()*1000);
      END;

    DECLARE CONTINUE HANDLER FOR 1050
      BEGIN
        INSERT INTO ramon2 VALUE (DEFAULT,0);
      END;
    CREATE TABLE ramon1(id int PRIMARY KEY);
    INSERT INTO ramon1 VALUES(1);
  END //
  DELIMITER ;

CALL errores5();
SELECT * FROM ramon1;
SELECT * FROM ramon2;


CREATE TABLE IF NOT EXISTS ejemplo1 (
  numero int(11) NOT NULL DEFAULT 0,
  valor varchar(10) NOT NULL,
  PRIMARY KEY (numero),
  UNIQUE INDEX valor (valor)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_spanish_ci;


CREATE TABLE IF NOT EXISTS fechas (
  fecha date DEFAULT NULL,
  texto varchar(255) DEFAULT NULL
)
ENGINE = INNODB
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_spanish_ci;


CREATE TABLE IF NOT EXISTS tabla (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  fecha date DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

DELIMITER $$

CREATE DEFINER = 'root'@'localhost'
PROCEDURE e1 ()

  FOR SQLSTATE '23000'
BEGIN

END

  CREATE TABLE ejemplo1 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;
END
$$

CALL e1();


CREATE DEFINER = 'root'@'localhost'
PROCEDURE e4 ()
BEGIN
  DECLARE EXIT HANDLER FOR 1050 SELECT
    'ya existe la tabla' AS error;
  CREATE TABLE ejemplo1 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;
END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE e5 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLSTATE '42S01' SELECT
    'ya existe la tabla' AS error;
  CREATE TABLE ejemplo1 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;
END
$$

CREATE DEFINER = 'root'@'localhost'
PROCEDURE e6 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT -- sqlexception salta siempre, controlar q es generico
    'ya existe la tabla' AS error;
  CREATE TABLE ejemplo1 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;
END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE e7 ()
BEGIN
  DECLARE CONTINUE HANDLER FOR 1051 SELECT
    'NO ENCUENTRO LA TABLA' AS ERROR;
  DECLARE EXIT HANDLER FOR 1050 SELECT
    'ya existe la tabla' AS error;

  DROP TABLE ejemplo1;
  CREATE TABLE ejemplo1 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;
END
$$


    -- creando condiciones (condition)
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e8 ()
BEGIN
  DECLARE bien int DEFAULT 1;
  DECLARE codigo int DEFAULT 0;
      -- creando los grupos
  DECLARE error1 CONDITION FOR 1050;
  DECLARE error2 CONDITION FOR 1051;
      -- creando 
  DECLARE CONTINUE HANDLER FOR error1 SET codigo = 1, bien = 0;
  DECLARE CONTINUE HANDLER FOR error2 SET codigo = 2, bien = 0;

  DROP TABLE ejemplo31;
  CREATE TABLE ejemplo2 (
    numero int,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE KEY (valor)
  ) ENGINE = INNODB;

  IF (bien = 0) THEN
    SELECT
      'algo mal' AS errores;
    IF (codigo = 1) THEN
      DROP TABLE ejemplo2;
    ELSEIF (codigo = 2) THEN
      CREATE TABLE ejemplo31 (
        numero int,
        valor varchar(10) NOT NULL,
        PRIMARY KEY (numero),
        UNIQUE KEY (valor)
      ) ENGINE = INNODB;
    END IF;
  END IF;

END
$$

CREATE DEFINER = 'root'@'localhost'
PROCEDURE e9 (arg1 int, arg2 varchar(10))
BEGIN
  DECLARE bien int DEFAULT 1;
  DECLARE numero int DEFAULT 0;
  DECLARE error2 CONDITION FOR 1048;

  BEGIN
    DECLARE error1 CONDITION FOR 1062;



    DECLARE CONTINUE HANDLER FOR error2
    BEGIN
      SELECT
        MYSQL_ERRNO;
      IF (arg1 IS NULL) THEN
        SELECT
          'el campo numero es requerido y no admite nulos' AS errores;
      END IF;
      IF (arg2 IS NULL) THEN
        SELECT
          'el campo valor es requerido y no admite nulos' AS errores;
      END IF;
    END;


    DECLARE CONTINUE HANDLER FOR error1
    BEGIN

      SELECT
        COUNT(*) INTO numero
      FROM ejemplo1 e
      WHERE e.numero = arg1;
      IF (numero = 1) THEN
        SELECT
          'El campo numero es clave principal y no admite valores repetidos' AS errores;
      END IF;
      SELECT
        COUNT(*) INTO numero
      FROM ejemplo1 e
      WHERE e.valor = arg2;
      IF (numero = 1) THEN
        SELECT
          'El campo valor es indexado sin duplicados y no admite valores repetidos' AS errores;
      END IF;
    END;
  END;

  INSERT INTO ejemplo1
    VALUES (arg1, arg2);


END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo1 ()
BEGIN
  DECLARE variable int;
  DECLARE CONTINUE HANDLER FOR 1048 SELECT
    'error1'; -- valor requerido
  DECLARE CONTINUE HANDLER FOR 1062 SELECT
    'error'; -- valor duplicado  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SELECT
    'no esta'; -- no se encuentra

  CREATE TEMPORARY TABLE t1 (
    numero int,
    nombre varchar(10) NOT NULL,
    PRIMARY KEY (numero)
  );

  INSERT INTO t1
    VALUES (1, '1'), (2, '2'), (3, '0');
  INSERT INTO t1
    VALUES (1, ''), (10, NULL);

  SELECT
    numero INTO variable
  FROM t1 t
  WHERE t.numero = 100;

END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo2 ()
BEGIN
  DECLARE EXIT HANDLER FOR NOT FOUND SELECT
    'no esta'; -- no se encuentra

  CREATE TEMPORARY TABLE t1 (
    numero int,
    nombre varchar(10) NOT NULL,
    PRIMARY KEY (numero)
  );

  DELETE
    FROM t1
  WHERE numero = a;

END
$$

DELIMITER $$
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo3 (pval int)
BEGIN

  DECLARE s CONDITION FOR SQLSTATE '45000';

  IF (pval = 0) THEN
    SIGNAL SQLSTATE '01000';
  ELSEIF (pval = 1) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ha ocurrido un error';
  ELSEIF (pval = 2) THEN
    SIGNAL s SET MESSAGE_TEXT = 'ha ocurrido un error';
  ELSE
    SIGNAL SQLSTATE '01000' SET MESSAGE_TEXT = 'ha ocurrido un warning', MYSQL_ERRNO = 1000;
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ha ocurrido un warning', MYSQL_ERRNO = 1001;
  END IF;

END
$$

CALL ejemplo3(1);

CREATE OR REPLACE TABLE alumnos(
  id int AUTO_INCREMENT PRIMARY KEY,
  edad int
  );

INSERT INTO alumnos VALUE(DEFAULT,15);
INSERT INTO alumnosa VALUE(DEFAULT,5);
SELECT * FROM alumnos;

DELIMITER $$
CREATE OR REPLACE PROCEDURE meterAlumno(edad int)
  BEGIN
    -- control de excepciones
    /* DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
      BEGIN
        SELECT 'edad no valida';  -- mejor no hacer un continue handler, entra en bucle y el usuario este mensaje no lo ve
      END; */
  -- programa normal
  IF(edad<=10) THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT='Edad debe ser mayor que 10';    
  END IF;
    INSERT INTO alumnos VALUE (DEFAULT,edad);
  END $$
DELIMITER ;

CALL meterAlumno(5);
CALL meterAlumno(500);
SELECT * FROM alumnos;



CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo4 ()
BEGIN
  CREATE TEMPORARY TABLE uno (
    numero int PRIMARY KEY
  ) ENGINE = INNODB;

  INSERT INTO uno
    VALUES (1), (2), (1);
END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo5 (divisor int)
BEGIN
  IF (divisor = 0) THEN
    SIGNAL SQLSTATE '04012' SET MESSAGE_TEXT = ' no podemos poner un cero para dividir';
  END IF;

  SELECT
    5 / divisor;
END
$$


CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejemplo6 (divisor int)
BEGIN
  DECLARE dividirPorCero CONDITION FOR SQLSTATE '01000';
  IF (divisor = 0) THEN
    SIGNAL dividirPorCero SET MESSAGE_TEXT = ' no podemos poner un cero para dividir', MYSQL_ERRNO = 5;
  END IF;

  SELECT
    5 / divisor;
END
$$

CREATE DEFINER = 'root'@'localhost'
FUNCTION nueve (fecha date)
RETURNS varchar(255) charset utf8 COLLATE utf8_spanish_ci
BEGIN
  DECLARE dia varchar(255);

  DROP TEMPORARY TABLE IF EXISTS temporal;
  CREATE TEMPORARY TABLE temporal (
    id int,
    nombre varchar(50)
  );

  INSERT INTO temporal
    VALUES (1, 'domingo'),
    (2, 'lunes'),
    (3, 'martes'),
    (4, 'miercoles'),
    (5, 'jueves'),
    (6, 'viernes'),
    (7, 'sabado');

  SET dia = (SELECT
    t.nombre
  FROM temporal t
  WHERE t.id = DAYOFWEEK(fecha));

  RETURN dia;

END
$$

--
-- Definition for function seis
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION seis (fecha date)
RETURNS varchar(255) charset utf8 COLLATE utf8_spanish_ci
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha, 'laboral');
  END;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha, 'festivo');
  END;

    CASE DAYOFWEEK(fecha)
      WHEN 1 THEN SIGNAL SQLSTATE '01001';
      RETURN 'domingo';
      WHEN 2 THEN SIGNAL SQLSTATE '01000';
      RETURN 'lunes';
      WHEN 3 THEN SIGNAL SQLSTATE '01000';
      RETURN 'martes';
      WHEN 4 THEN SIGNAL SQLSTATE '01000';
      RETURN 'miercoles';
      WHEN 5 THEN SIGNAL SQLSTATE '01000';
      RETURN 'jueves';
      WHEN 6 THEN SIGNAL SQLSTATE '01000';
      RETURN 'viernes';
      WHEN 7 THEN SIGNAL SQLSTATE '01001';
      RETURN 'sabado'; ELSE RETURN 'ninguno';
    END CASE;

END
$$

DELIMITER ;

-- 
-- Dumping data for table ejemplo1
--
INSERT INTO ejemplo1 VALUES
(1, '1'),
(2, '1111111111'),
(125, '2');

-- 
-- Dumping data for table fechas
--
INSERT INTO fechas VALUES
('2015-05-04', 'laboral'),
('2015-05-09', 'festivo'),
('2015-04-17', 'laboral');

-- 
-- Dumping data for table tabla
--
INSERT INTO tabla VALUES
(1, 'ramon', '2015-05-04'),
(2, 'jose', '2015-05-09'),
(3, 'silvia', '2015-04-17');

