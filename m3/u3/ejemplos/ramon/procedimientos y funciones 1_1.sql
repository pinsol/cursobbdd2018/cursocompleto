﻿/*
  Ejemplos de procedimientos almacenados y funciones
*/

CREATE DATABASE procedimientosFunciones1;
USE procedimientosFunciones1;


/* 
  crear un procedimiento almacenado 
  muestre la fecha de hoy
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej1 //
CREATE PROCEDURE ej1()
BEGIN
    SELECT NOW();
END //
DELIMITER ;

/* llamar al procedimiento almacenado */

CALL ej1();


/* 
  Ej2
  crear una variable de tipo datetime
  y almacenar en esa variable la 
  fecha de hoy
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej2 //
CREATE PROCEDURE ej2()
BEGIN
  DECLARE v1 datetime; -- declarando
  SET v1=NOW(); -- asignado
  SELECT v1;
END //
DELIMITER ;

/* llamo al procedimiento almacenado */

CALL ej2();


/* SUMA 2 NUMEROS 
    que recibe como argumentos  
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3 //
CREATE PROCEDURE ej3(arg1 int, arg2 INT)
BEGIN
  DECLARE suma INT;
  SET suma=arg1+arg2;
  SELECT suma;
END //
DELIMITER ;

/* llamar al procedimiento */
CALL ej3(3,4);
CALL ej3(6,4);
CALL ej3(7,4);

/* Reste 2 NUMEROS 
    que recibe como argumentos de 
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3A //
CREATE PROCEDURE ej3A(arg1 int, arg2 INT)
BEGIN
  DECLARE resta INT;
  SET resta=arg1-arg2;
  SELECT resta;
END //
DELIMITER ;

/* llamar al procedimiento */
CALL ej3A(3,4);
CALL ej3A(6,4);
CALL ej3A(7,4);

/*
  Ej31
  crear un procedimiento que sume dos numeros y los 
  almacene en una tabla llamada datos.
  La tabla la debe crear en caso de que no
  exista.
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej31 //
CREATE PROCEDURE ej31(a1 int, a2 INT)
BEGIN
  -- declarar las variables
  DECLARE suma int DEFAULT 0;

  -- crear la tabla para almacenar los resultados
  CREATE TABLE IF NOT EXISTS datos(
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    suma int,
    PRIMARY KEY(id)
    );

  -- calcule la suma
  set suma=a1+a2;

  -- introducir los datos en la tabla
  INSERT INTO datos VALUES
    (DEFAULT,a1,a2,suma);

END //
DELIMITER ; 

/* 
  probar el procedimiento
*/

  CALL ej31(12,5);
  SELECT * FROM datos;
  CALL ej31(12,15);

/* 
  Ej32
  SUMA, producto 2 NUMEROS 
  Crear una tabla llamada ej32(id,d1,d2,suma,producto)
  introducir los datos en la tabla y los resultados
*/
DELIMITER //
DROP PROCEDURE IF EXISTS ej32 //
CREATE PROCEDURE ej32(arg1 int, arg2 INT)
BEGIN
  -- crear variables
  DECLARE suma int DEFAULT 0;
  DECLARE producto int DEFAULT 0;

  -- establecer valor
  SET suma=arg1+arg2;
  SET producto=arg1*arg2;

  -- crear la tabla para almacenar los datos
  CREATE TABLE if NOT EXISTS ej32(
    id int AUTO_INCREMENT,
    n1 int,
    n2 int,
    suma int,
    producto int,
    PRIMARY KEY(id)
  );

  -- almacenado el dato en la tabla
  INSERT INTO ej32 
    VALUES (DEFAULT,arg1,arg2,suma,producto);
END //
DELIMITER ;

/* llamar al procedimiento */
  CALL ej32(2,6);
  SELECT * FROM ej32 e;
  CALL ej32(12,16);


/* 
  procedimiento ej33()
  crear una tabla llamada ej33 si existe la elimina
*/

-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej33 //

CREATE PROCEDURE ej33()
BEGIN
  DROP TABLE IF EXISTS ej33;
  CREATE TABLE ej33(
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    resultado int,
    PRIMARY KEY(id)
    );
END //
DELIMITER ;

-- probar mi procedimiento
CALL ej33();


/*
  Ej33A
  objetivo: elevar un numero a otro. Almacenar
  el resultado y los numeros en la tabla ej33
  argumentos: base y el exponente
*/
  -- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej33A //

CREATE PROCEDURE ej33A(numero1 int, numero2 int)
BEGIN
  -- creando la variable
  DECLARE resultado int DEFAULT 0;

  -- realizo la potencia
  SET resultado=POW(numero1,numero2);

  -- almaceno el dato en la tabla
  INSERT INTO ej33 VALUE
    (DEFAULT,numero1,numero2,resultado);

END //
DELIMITER ;

-- probando el procedimiento

CALL ej33A(5,3);
SELECT * FROM ej33;

/*
  Ej33B
  objetivo: realizar la raiz cuadrada de un numero
  Almacenar el numero y su raiz en la tabla ej33
*/

  -- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej33B //

CREATE PROCEDURE ej33B(numero1 int)
BEGIN
  -- creando la variable
  DECLARE resultado int DEFAULT 0;

  -- realizo la potencia
  SET resultado=SQRT(numero1);

  -- almaceno el dato en la tabla
  INSERT INTO ej33 VALUE
    (DEFAULT,numero1,NULL,resultado);

END //
DELIMITER ;

-- probando el procedimiento
CALL ej33B(81);
SELECT * FROM ej33;


/*
  Ej34
  Crea una tabla y si existe la elimina y la crea
  La tabla se llama ej34(id,texto,longitud,caracteres)
*/

  -- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej34 //

CREATE PROCEDURE ej34()
BEGIN
  DROP TABLE IF EXISTS ej34;
  CREATE TABLE ej34(
    id int AUTO_INCREMENT,
    texto varchar(50),
    longitud int,
    caracteres varchar(50),
    PRIMARY KEY(id)
    );
END //
DELIMITER ;

-- probar el procedimiento 
 CALL ej34();

/*
  Ej34B
  Objetivo: Introducir un registro nuevo en la 
  tabla ej34
  Argumentos: texto a introducir en la tabla

*/

-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej34B //

CREATE PROCEDURE ej34B(argumento varchar(50))
BEGIN
  INSERT INTO ej34(texto) VALUE 
    (argumento);
END //
DELIMITER ;

-- probar 
CALL ej34b('Ejemplo de clase');
SELECT * FROM ej34;

/*
  EJ34A
*/

  -- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej34C //

CREATE PROCEDURE ej34C(numeroCaracteres int, registro int)
BEGIN
  UPDATE 
    ej34 JOIN 
    (SELECT * FROM ej34 LIMIT registro,1000) c1
    USING(id)
    SET ej34.caracteres=LEFT(ej34.texto,numeroCaracteres);
END //
DELIMITER ;

-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej34A //

CREATE PROCEDURE ej34A()
BEGIN

  UPDATE ej34 
    SET longitud=LENGTH(texto);
  
END //
DELIMITER ;

/*
  Eje35

*/

-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej35 //

CREATE PROCEDURE ej35(numero int)
BEGIN

  -- SELECT IF(numero>10,"grande","pequeño");

  IF (numero>10) THEN 
    SELECT "grande";
  ELSE
    SELECT "pequeño";
  END IF;

END //
DELIMITER ;

CREATE TEMPORARY TABLE ejemplo(
  id int AUTO_INCREMENT,
  edad int,
  PRIMARY KEY (id)
  );
SELECT * FROM ejemplo e;


/* 
  ejemplo 4 

*/

-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej4 //

CREATE PROCEDURE ej4(n1 int, n2 int)
BEGIN
    
  CREATE OR REPLACE TEMPORARY TABLE ej4(
    indice int AUTO_INCREMENT,
    valor int,
    PRIMARY KEY (indice)
  );
  
  -- SET ej4=n1;
  INSERT INTO ej4(valor) VALUES
    (n1),
    (n2);

  SELECT valor FROM ej4 ORDER BY valor desc;

  /*IF (n1>n2) THEN
    SELECT n1,n2;
  ELSE
    SELECT n2,n1;
  END IF;*/

END //
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS ej4 //
CREATE PROCEDURE ej4(arg1 int, arg2 INT)
BEGIN
  CREATE TEMPORARY TABLE ej4(
    id int AUTO_INCREMENT,
    numero int,
    PRIMARY KEY (id)
  );

  IF (arg1>arg2) THEN
    INSERT INTO ej4 VALUES 
      (DEFAULT,arg1),
      (DEFAULT,arg2);
  ELSE 
    INSERT INTO ej4 VALUES 
      (DEFAULT,arg2),
      (DEFAULT,arg1);
  END IF;

  SELECT numero FROM ej4 e;
END //
DELIMITER ;

/* llamar procedimiento */
CALL ej4(23,67);


-- crear el procedimiento
DELIMITER //
DROP PROCEDURE IF EXISTS ej5 //

CREATE PROCEDURE ej5(arg1 int,OUT arg2 int,INOUT arg3 int)
BEGIN
  SELECT arg1,arg2,arg3;
  set arg1=arg1+50;
  set arg2=100;
  set arg3=arg3+500;
END //
DELIMITER ;

SET @numero1=1,@numero3=10, @numero2=24;
CALL ej5(@numero1,@numero2,@numero3);
SELECT @numero1,@numero2,@numero3;


/* CREAR EL PROCEDIMIENTO */
DELIMITER //
DROP PROCEDURE IF EXISTS ej5 //
CREATE PROCEDURE ej5(IN arg1 int,OUT arg2 int,INOUT arg3 int)
BEGIN
  DECLARE v1 int;
  DECLARE v2 int DEFAULT 0;
  SET v1=arg1+arg3;

  CREATE TABLE IF NOT EXISTS ej5(
    c1 int,
    c2 int,
    c3 int,
    PRIMARY KEY (c1)
    );
  
  INSERT INTO ej5 VALUE (arg1,arg3,v1);

  set arg2=(SELECT COUNT(*) FROM ej5);
  SELECT COUNT(*) INTO arg2 FROM ej5 ;
    
END //
DELIMITER ;

/* FIN DE LA CREACION DEL PROCEDIMIENTO */

/* LLAMAR AL PROCEDIMIENTO */
SET @e2=0;
CALL ej5(12,@e1,@e2);
SELECT @e1;


DELIMITER $$
DROP PROCEDURE IF EXISTS ej7$$
CREATE PROCEDURE ej7 (n1 int)
BEGIN
    CREATE TABLE if NOT EXISTS ej7(
      id int AUTO_INCREMENT,
      c1 int,
      c2 float,
      c3 int,
      PRIMARY KEY(id)
    );

    IF (n1>=0 AND n1<=10) THEN
      INSERT IGNORE INTO ej7 
        VALUE (DEFAULT,n1,0.5,DAY(CURRENT_DATE()));
    ELSEIF (n1>10 AND n1<=20) THEN 
      INSERT IGNORE INTO ej7 
        VALUE (DEFAULT,n1,0.7,DAY(CURRENT_DATE()));
    ELSEIF (n1>20 AND n1<=50) THEN
      INSERT IGNORE INTO ej7 
        VALUE (DEFAULT,n1,1,DAY(CURRENT_DATE()));
    END IF;

END
$$

DELIMITER ;


DROP DATABASE 
  IF EXISTS practicaapoyo2;

CREATE DATABASE 
  practicaapoyo2;

USE practicaapoyo2;

DELIMITER $$

-- procedimiento almacenado 1
  /* 
  con este procedimiento vamos a mostrar en pantalla numeros desde el 1 al 10 
  */
DROP PROCEDURE IF EXISTS practicaapoyo2.p1$$
CREATE PROCEDURE practicaapoyo2.p1 ()
BEGIN
  -- creamos una variable
  DECLARE contador INT DEFAULT 1;
  -- creamos un bucle
  WHILE (contador<=10) DO
    -- muestra la variable
    SELECT contador;
    -- incrementamos la variable en 1
    SET contador = contador + 1 ;
  END WHILE;

END
$$

DELIMITER ;

DELIMITER $$

-- procedimiento almacenado 2
  /* 
  Modificar el procedimiento para que muestre numeros desde 1 hasta 20
  */
DROP PROCEDURE IF EXISTS practicaapoyo2.p2$$
CREATE PROCEDURE practicaapoyo2.p2 ()
BEGIN
  -- creamos una variable
  DECLARE contador INT DEFAULT 1;
  -- creamos un bucle
  WHILE (contador<=20) DO
    -- muestra la variable
    SELECT contador;
    -- incrementamos la variable en 1
    SET contador = contador + 1 ;
  END WHILE;

END
$$

DELIMITER ;

DELIMITER $$

-- procedimiento almacenado 3
  /* 
  con este procedimiento vamos a mostrar en pantalla numeros desde el 1 hasta el numero que introduzcamos por teclado
  */
DROP PROCEDURE IF EXISTS practicaapoyo2.p3$$
-- le pasamos al procedimiento almacenado 1 argumento de tipo entero
CREATE PROCEDURE practicaapoyo2.p3 (IN numero int)
BEGIN
  -- creamos una variable
  DECLARE contador INT DEFAULT 1;
  -- creamos un bucle
  WHILE (contador<=numero) DO
    -- muestra la variable
    SELECT contador;
    -- incrementamos la variable en 1
    SET contador = contador + 1 ;
  END WHILE;

END
$$

DELIMITER ;


DELIMITER $$
-- procedimiento almacenado 4
  /* rfffff
  con este procedimiento vamos a mostrar en pantalla numeros desde el 1 numero pasado por teclado hasta otro numero que introduzcamos por teclado
  */
  --  DEBEIS CORREGIR UN ERROR QUE DA EL PROCEDIMIENTO ALMACENADO
DROP PROCEDURE IF EXISTS practicaapoyo2.p4$$
CREATE PROCEDURE practicaapoyo2.p4 (IN inicio int, IN fin int)
BEGIN
  -- creamos una variable y le asignamos el valor por defecto que es el argumento inicio pasado como argumento
  DECLARE contador INT DEFAULT inicio;
  -- creamos un bucle
  WHILE (contador<=numero) DO
    -- muestra la variable
    SELECT contador;
    -- incrementamos la variable en 1
    SET contador = contador + 1 ;
  END WHILE;

END
$$


DELIMITER ;


DELIMITER $$
-- procedimiento almacenado 5
  /*
    Grabamos numeros desde el 1 al 10 en una tabla  
  */
  
DROP PROCEDURE IF EXISTS practicaapoyo2.p5$$

CREATE PROCEDURE practicaapoyo2.p5 ()
BEGIN
  -- creacion de variables
  DECLARE contador INT DEFAULT 1;
  
  DROP TABLE IF EXISTS t1;
  CREATE TABLE t1
    (
      numeros int
    );

  
  WHILE (contador<=10) DO
    INSERT INTO t1 VALUES (contador);
    SET contador = contador + 1 ;
  END WHILE;

END
$$

DELIMITER ;

DELIMITER $$
-- procedimiento almacenado 6
  /*
    DEBEIS MODIFICAR EL PROCEDIMIENTO ANTERIOR PARA QUE EL NUMERO DE INICIO Y DE FIN SE PASEN COMO ARGUMENTO.
    ES DECIR SI LLAMO AL PROCEDIMIENTO COMO P6(3,5) INTRODUCIRA LOS NUMEROS 3,4,5 Y 6 EN LA TABLA.
  */
  
DROP PROCEDURE IF EXISTS practicaapoyo2.p6$$

CREATE PROCEDURE practicaapoyo2.p6 ()
BEGIN
  -- creacion de variables
  DECLARE contador INT DEFAULT 1;
  
  DROP TABLE IF EXISTS t1;
  CREATE TABLE t1
    (
      numeros int
    );

  
  WHILE (contador<=10) DO
    INSERT INTO t1 VALUES (contador);
    SET contador = contador + 1 ;
  END WHILE;

END
$$

DELIMITER ;

DELIMITER $$

-- procedimiento almacenado 7
  /*
    ¿QUE ES LO QUE REALIZA EL SIGUIENTE PROCEDIMIENTO?
  */
  
DROP PROCEDURE IF EXISTS practicaapoyo2.p7$$

CREATE PROCEDURE practicaapoyo2.p7 (IN parameter1 int)
BEGIN
 DECLARE variable1 INT;

  DROP TABLE If EXISTS t1;
  CREATE TABLE t
    (
      s1 int
    );

 SET variable1 = parameter1 + 1;
 
 IF (variable1 = 0) THEN
  INSERT INTO t VALUES (17);
 END IF;
 IF (parameter1 = 0) THEN
  UPDATE t SET s1 = s1 + 1;
 ELSE
  UPDATE t SET s1 = s1 + 2;
 END IF; 

END
$$

DELIMITER ;

DELIMITER $$

-- procedimiento almacenado 8
  /*
    CORREGIR EL SIGUIENTE PROCEDIMIENTO ALMACENADO.
    ¿QUE INTENTA REALIZAR?
  */
  
DROP PROCEDURE IF EXISTS practicaapoyo2.p8$$

CREATE PROCEDURE practicaapoyo2.p8 (IN p int)
BEGIN
 
  DECLARE v INT;
  DROP TABLE If EXISTS t1;
  CREATE TABLE t
    (
      s1 int
    );


 SET v = 0;
 
 REPEAT
  INSERT INTO t VALUES (v);
  SET v = v + 1;
 UNTIL (v >= 5)
 END REPEAT; 

END
$$

DELIMITER ;

-- procedimiento almacenado 9
  /*
    
  */

DELIMITER $$
  
DROP PROCEDURE IF EXISTS practicaapoyo2.p9$$

CREATE PROCEDURE practicaapoyo2.p9 (IN descuento float)
BEGIN
  DROP TABLE IF EXISTS t;
 
  CREATE TABLE t(
    id int AUTO_INCREMENT,
    unidades int,
    precioUnitario float,
    precioFinal float,
    poblacion varchar(20),
    PRIMARY KEY (id)
  );

  INSERT INTO t VALUES(DEFAULT,10,20,0,'santander'),(DEFAULT,1,30,0,'santander'),(DEFAULT,2,23,0,'potes'),(DEFAULT,1,200,0,'laredo'),(DEFAULT,4,50,0,'santander');
  
  SET v = 0;
 
 REPEAT
  INSERT INTO t VALUES (v);
  SET v = v + 1;
 UNTIL (v >= 5)
 END REPEAT; 

END
$$

DELIMITER ;