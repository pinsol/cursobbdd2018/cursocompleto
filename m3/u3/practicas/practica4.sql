﻿/*                              Practica 4                                          */
DROP DATABASE practica4apoyo;
CREATE DATABASE practica4apoyo;
USE practica4apoyo;

/* Ejercicio 1: Crea un procedimiento que reciba una cadena que puede contener letras y números
                y sustituya los números por *. El argumento debe ser único y almacenar en él, el resultado */

DELIMITER //
CREATE OR REPLACE PROCEDURE ejerc4_1(INOUT cadena varchar(20))
BEGIN

DECLARE ini int DEFAULT 0;
DECLARE fin int DEFAULT 9;
DECLARE cont int DEFAULT ini;


WHILE (cont<=fin) DO
  set cadena = REPLACE(cadena,cont,'*');
  set cont=cont+1;
END WHILE;

END //
DELIMITER ;

set @a="ejem32 ";
CALL ejerc4_1(@a);
SELECT @a;

/* Ejercicio 2: Crea una función que recibe como argumento una cadena que puede contener letras y números
                y debe devolver la cadena con el primer carácter en mayúsculas y el resto en minúculas */

DELIMITER //
CREATE OR replace FUNCTION ejerc4_2(cadena varchar(25))
  RETURNS varchar(25)
  BEGIN
  RETURN (SELECT CONCAT(UPPER(SUBSTR(cadena,1,1)),SUBSTR(cadena,2)));
  END //
DELIMITER ;

SELECT ejerc4_2('piter');


/* Ejercicio 3: Desarrollar una función que devuelva el número de años completos que hay entre dos fechas
                que se pasan como parámetros. Utiliza la función DATEDIFF */

DELIMITER //
CREATE OR REPLACE FUNCTION ej3(fecha1 date, fecha2 date)
  RETURNS int
  BEGIN
  RETURN (SELECT FLOOR(ABS(DATEDIFF(fecha1, fecha2))/365));
  END //
DELIMITER ;

SELECT ej3('2017-02-17','2015-03-25');



/* Ejercicio 4. Realiza el ejercicio anterior pero utilizando la función TIMESTAMPDIFF. */

    
DELIMITER //
CREATE OR REPLACE FUNCTION ej4(fecha1 date, fecha2 date)
  RETURNS int
  BEGIN
  RETURN (SELECT TIMESTAMPDIFF(year,fecha1,fecha2));
  END //
DELIMITER ;

SELECT ej4('2015-02-17','2019-03-25');



/* Ejercicio 5. Crear un procedimiento que genere una tabla llamada números(un solo campo de tipo int) la rellene de 50 números y después devuelva la suma,
  media, valor máximo y valor mínimo. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej5()
  BEGIN
DECLARE suma int;
DECLARE media int;
DECLARE maximo int;
DECLARE minimo int;
DECLARE contador int DEFAULT 0;

CREATE OR REPLACE TEMPORARY TABLE numeros(
  numero int
  );

WHILE
  contador <50
DO
 INSERT INTO numeros (numero)
  VALUES (RAND()*100);
 SET contador = contador +1;
END WHILE;

SET suma=(SELECT SUM(numero) FROM numeros);
set media=(SELECT AVG(numero) FROM numeros);
SET maximo=(SELECT MAX(numero) FROM numeros);
SET minimo=(SELECT MIN(numero) FROM numeros);

SELECT suma, media, maximo, minimo;      

  END //
DELIMITER ;

CALL ej5();


/* Ejercicio 6. Crear una función que reciba 5 números y devuelva su valor máximo, mínimo, media y suma. */

  DELIMITER //
CREATE OR REPLACE FUNCTION ej6(valor1 int, valor2 int, valor3 int, valor4 int, valor5 int)
  RETURNS varchar (20)
  BEGIN
  DECLARE maximo int;
  DECLARE minimo int;
  DECLARE media float;
  DECLARE suma int;
  DECLARE resultado varchar (20);

  set maximo=greatest(valor1, valor2, valor3, valor4, valor5);
  SET minimo= least(valor1, valor2, valor3, valor4, valor5);
  SET media= (valor1+valor2+valor3+valor4+valor5)/5;
  SET suma= (valor1+valor2+valor3+valor4+valor5);
  set resultado =  CONCAT(minimo,' ', media, ' ', suma,' ', maximo);
  RETURN resultado;
  END //
DELIMITER ;

SELECT ej6(4,6,1,9,5);



-- otra forma de hacerlo
  DELIMITER //
CREATE OR REPLACE FUNCTION ej6a(valor1 int, valor2 int, valor3 int, valor4 int, valor5 int)
  RETURNS varchar(20)
  BEGIN
  DECLARE maximo int;
  DECLARE minimo int;
  DECLARE media float;
  DECLARE suma int;
  DECLARE resultado varchar (20);
  CREATE OR REPLACE TEMPORARY TABLE numeros2(
  numero int);

  INSERT INTO numeros2 (numero) VALUES (valor1), (valor2), (valor3), (valor4), (valor5);

  set maximo=(SELECT MAX(numero) FROM numeros2);
  SET minimo= (SELECT MIN(numero) FROM numeros2);
  SET media= (SELECT AVG(numero) FROM numeros2);
  SET suma= (SELECT SUM(numero) FROM numeros2);
  set resultado =  CONCAT(minimo,' ', media, ' ', suma,' ', maximo);
  RETURN resultado;
  END //
DELIMITER ;

SELECT ej6a(1,5,7,9,12);


/* Ejercicio 7. Realizar un procedimiento que genere una tabla con dos campos (nota de tipo int, resultado de tipo varchar).
  Debe rellenar esa tabla con 10 números entre 0 y 10 (con el procedimiento). */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7 ()
  BEGIN
DECLARE contador int DEFAULT 0;

CREATE OR REPLACE TEMPORARY TABLE tablae7(
  nota int,
  resultado varchar(20)
  );


WHILE contador <10
  DO
  INSERT INTO tablae7(nota) VALUES (RAND()*10);
  set contador=contador+1;
  END WHILE ;

  END //
DELIMITER ;

CALL ej7();
SELECT * FROM tablae7;

/* Ejercicio 8. Realizar un procedimiento almacenado que almacene en el campo resultado el siguiente valor en función de nota:
  a. Nota entre 0 y 4: suspenso
  b. Nota 5: Suficiente.
  c. Nota 6: Bien.
  d. Nota entre 7 y 8: Notable.
  e. Nota entre 9 y 10: Sobresaliente.
  */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej8()
  BEGIN
UPDATE tablae7
  SET resultado= IF(nota<5, 'suspenso', IF(nota<6, 'suficiente', IF(nota<7, 'bien', IF(nota<9,'notable', IF(nota<=10, 'sobresaliente', 'nota no válida')))));

  END //
DELIMITER ;

CALL ej8();
SELECT * FROM tablae7;

/* Ejercicio 9. Crear un procedimiento almacenado que genere estas dos tablas. */
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej9()
  BEGIN

CREATE OR REPLACE TEMPORARY TABLE comisiones(
  vendedor int,
  comision float);
CREATE OR REPLACE TEMPORARY TABLE ventas(
  vendedor int,
  producto int,
  importe float);

  END //
DELIMITER;

CALL ej9();


/* Ejercicio 10. Analizar el siguiente procedimiento almacenado. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej10(IN mivendedor int)
BEGIN
  DECLARE micomision int DEFAULT 0;
  DECLARE suma int ;
  DECLARE existe bool;
  SELECT
    IFnull (SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 1 AND vendedor = mivendedor;
  SET micomision=micomision+(suma*0.15);
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM VENTAS
  WHERE producto=2 AND vendedor = mivendedor;
  SET micomision=micomision+(suma*0.1);
  SELECT
    IFNULL(SUM(importe),0) INTO suma
  FROM ventas
  WHERE producto=3 AND vendedor = mivendedor;
  SET micomision=micomision+(suma*0.2);

  /* AQUI VA EL EJERCICIO 14. Modificar el procedimiento almacenado para que en caso de que sea un producto de tipo 4 le coloquemos una comisión del 30% de sus ventas
  sobre ese producto.
  */
SELECT
    IFNULL(SUM(importe),0) INTO suma
  FROM ventas
  WHERE producto=4 AND vendedor = mivendedor;
  SET micomision=micomision+(suma*0.3);

  SELECT
    COUNT(1)>0 INTO existe
  FROM comisiones
  WHERE vendedor = mivendedor;
  IF existe THEN
    UPDATE comisiones
    set comision=comision+micomision
    WHERE vendedor=mivendedor;
  ELSE
    INSERT INTO comisiones(vendedor, comision)
      VALUES (mivendedor, micomision);
  END IF;
  END //
DELIMITER ;

  CALL ej10(1);
  CALL ej10(2);
  CALL ej10(3);
/* Ejercicio 11. Introducir una serie de registros para verificar el correcto funcionamiento del procedimiento almacenado. */
INSERT INTO ventas VALUES('1', '1', '15'), ('1', '2', '25'), ('2', '1', '15'), ('2', '3', '11'), ('3', '1', '15'),
('1', '4', '15'), ('1', '4', '25'), ('2', '4', '15'), ('2', '4', '11'), ('3', '4', '15');

SELECT * FROM comisiones;
