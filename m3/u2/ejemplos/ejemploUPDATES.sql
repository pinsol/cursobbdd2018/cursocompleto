﻿SELECT  nombre,fecha,p.dato1, p.dato2, p.dato3 FROM personas p;

/* coloque en dato1 un 1 a los que su nombre comience por 'C' */

   -- consulta de seleccion
  SELECT * FROM personas p WHERE LEFT(p.nombre,1)='c';

-- ¡¡¡ consulta de actualizacion !!!
UPDATE
  personas p 
SET
  p.dato1=1 
WHERE
 LEFT(p.nombre,1)='c'
;

/* en dato2 me vais a colocar el dia del mes de hoy si en dato1 hay un 1 */

    -- consulta de seleccion
    SELECT DAYOFMONTH(NOW()),p.* FROM personas p WHERE p.dato1=1;

-- ¡¡¡ CONSULTA DE ACCION !!!
UPDATE
  personas p
SET
  p.dato2=DAYOFMONTH(NOW())
WHERE
 p.dato1=1
;

/* en dato3 me debe colocar dato1 + dato2 si la fecha es del primer trimestre del año */

    -- consulta de seleccion
    SELECT * FROM personas p WHERE QUARTER(fecha)=1;

-- ¡¡¡ CONSULTA DE ACCION !!! 
UPDATE
  personas p
SET
  dato3=dato1+dato2
WHERE
 QUARTER(fecha)=1
;

/* poner 0 en el campo trabajadores */

  -- consulta de seleccion
  SELECT e.trabajadores FROM empresas e;

-- ¡¡¡ consulta de actualizacion !!!
UPDATE
  empresas e 
SET
  e.trabajadores=0
;

/* el campo trabajadores me indique cuantos trabajadores tiene esa empresa */
  
  -- consulta de seleccion
  SELECT empresa, COUNT(*) n FROM personas JOIN empresas e ON personas.empresa = e.codigo GROUP BY empresa;

-- ¡¡¡ consulta de actualizacion !!!
UPDATE empresas e JOIN
  (
  SELECT empresa,COUNT(*) n 
  FROM empresas JOIN personas
  ON empresas.codigo = personas.empresa
  GROUP BY empresa
  ) AS c1
  ON c1.empresa=e.codigo
  set trabajadores=c1.n
;

  SELECT * FROM empresas e JOIN
  (
  SELECT empresa,COUNT(*) n 
  FROM empresas JOIN personas
  ON empresas.codigo = personas.empresa
  GROUP BY empresa
  ) c1
  ON c1.empresa=e.codigo
  set trabajadores=c1.n
;