﻿/* realizamos la consulta en un paso */
SELECT
  equipo.*

FROM equipo
  JOIN ciclista c
    ON equipo.nomequipo = c.nomequipo
  JOIN etapa e
    ON c.dorsal = e.dorsal
  JOIN puerto p
    ON e.numetapa = p.numetapa;  -- no esta muy optimizada

/* realizamos la consulta por pasos */
-- c1
SELECT
  DISTINCT e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

-- c2
SELECT
  DISTINCT nomequipo

FROM ciclista c
  JOIN (SELECT
      e.numetapa,
      e.dorsal
    FROM etapa e
      JOIN puerto p
        ON e.numetapa = p.numetapa) AS c1
      ON c.dorsal=c1.dorsal;

-- consulta final
  SELECT 
    e.* 
  FROM
    equipo e
    JOIN (
      SELECT
      DISTINCT c.nomequipo
      FROM ciclista c
        JOIN (SELECT
           DISTINCT e.numetapa,
            e.dorsal
          FROM etapa e
           JOIN puerto p
            ON e.numetapa = p.numetapa) AS c1
            ON c.dorsal=c1.dorsal
            ) AS c2
              ON c2.nomequipo=e.nomequipo;

  CREATE OR REPLACE VIEW consulta1C1 AS
    SELECT
  DISTINCT e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

   CREATE OR REPLACE VIEW consulta1c2 AS
     SELECT
  DISTINCT nomequipo

FROM ciclista c
  JOIN consulta1c1 AS c1
      ON c.dorsal=c1.dorsal;

SELECT 
    e.* 
  FROM
    equipo e
    JOIN consulta1c2 AS c2
              ON c2.nomequipo=e.nomequipo;