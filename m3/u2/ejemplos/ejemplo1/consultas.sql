﻿/* Consulta de seleccion 1.1 */
SELECT
  DISTINCT edad -- PROYECCIÓN

FROM
ciclista; -- SELECCIÓN DE TABLA


/* Consulta de seleccion 1.2 */
SELECT
 DISTINCT edad -- PROYECCIÓN

FROM
 ciclista -- TABLA A UTILIZAR

WHERE
 nomequipo="artiach";  -- SELECCIÓN


/* Consulta de seleccion 1.3 */
SELECT
 DISTINCT edad

FROM
 ciclista

WHERE
 nomequipo="artiach"
 OR
 nomequipo="Amore Vita";


/* Consulta de seleccion 1.4 */
SELECT
 dorsal 

FROM
 ciclista 

WHERE
 edad BETWEEN 25 AND 30;


/* Consulta de seleccion 1.5 */
SELECT
 dorsal 

FROM
 ciclista 

WHERE
  edad BETWEEN 28 AND 32 
AND nomequipo="Banesto";


/* Consulta de seleccion 1.6 */
SELECT
 nombre 

FROM
 ciclista 

WHERE
 CHAR_LENGTH(nombre)>8;


/* Consulta de seleccion 1.7 */
SELECT
 nombre, dorsal, UPPER(RTRIM(nombre)) AS `Nombre Mayuscula`

FROM
 ciclista;


/* Consulta de seleccion 1.8 */
SELECT
 DISTINCT dorsal 

FROM
 lleva 

WHERE 
código="MGE";


/* Consulta de seleccion 1.9 */
SELECT
 nompuerto 

FROM
 puerto 

WHERE
 altura>1500;


/* Consulta de seleccion 1.10 */
SELECT
 dorsal 

FROM
  puerto 

WHERE
 pendiente>8
 OR
 altura BETWEEN 1800 AND 3000;


/* Consulta de seleccion 1.11 */
SELECT
 dorsal 

FROM
 puerto 

WHERE
 pendiente>8 
 AND
 altura BETWEEN 1800 AND 3000;