﻿/* Realizaremos SUBCONSULTAS CON VISTAS y utilizaremos DISTINTOS OPERADORES con ellas */

/*   OPERACIONES a realizar
    C3 - C1
    C3 - C2
    C4 - C3 - C2
    C1 U C3
    C1 U C3 U C4    */


                -- SUBCONSULTAS --

-- subconsulta C1: ciclistas que han llevado maillot
CREATE OR REPLACE VIEW consulta5C1
AS
SELECT
DISTINCT
  l.dorsal
FROM lleva l
;

-- subconsulta C2: ciclistas que NO han llevado maillot
CREATE OR REPLACE VIEW consulta5C2
AS
SELECT
DISTINCT
  c.dorsal
FROM ciclista c
  LEFT JOIN lleva l
    ON c.dorsal = l.dorsal
WHERE l.dorsal IS NULL
;

-- subconsulta C3: ciclistas que han ganado etapa
CREATE OR REPLACE VIEW consulta5C3
AS
SELECT
DISTINCT
  e.dorsal
FROM etapa e
;

-- subconsulta C3: ciclistas que han ganado puerto
CREATE OR REPLACE VIEW consulta5C4
AS
SELECT
DISTINCT
  p.dorsal
FROM puerto p
;



                -- OPERACIONES --

  -- C3 - C1
  SELECT
    C3.dorsal
  FROM consulta5C3 C3
    LEFT JOIN consulta5C1 C1 USING (dorsal)
  WHERE C1.dorsal IS NULL
  ;
  -- C3 - C2
  SELECT
    DISTINCT C3.dorsal
  FROM
    consulta5C3 C3
  WHERE
     C3.dorsal NOT IN (SELECT * FROM consulta5C2 C2)
  ;

  -- C4 - C3 - C2
  SELECT
    C4.dorsal
  FROM consulta5C4 C4
    LEFT JOIN consulta5C3 C3 USING (dorsal)
    LEFT JOIN consulta5C2 C2 USING (dorsal)
  WHERE C2.dorsal IS NULL 
        AND
        C3.dorsal IS NULL
  ;

  --  C1 U C3
  SELECT * FROM  consulta5C1
   UNION
  SELECT * FROM  consulta5C3
  ;

  --  C1 U C3 U C4
  SELECT * FROM  consulta5C1
   UNION
  SELECT * FROM  consulta5C3
   UNION
  SELECT * FROM  consulta5C4
  ;