﻿-- natural join -> une x campo q se llamen igual
  SELECT * FROM ciclista c NATURAL JOIN etapa e;

-- inner join -> une x el campo indica en ON
  SELECT * FROM ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal;

-- using -> join con el campo indicado con el mismo nombre
  SELECT * FROM  ciclista c INNER JOIN etapa e USING (dorsal);

-- teoricamente el join hace esta funcion (producto cartesiano)
  SELECT * FROM  ciclista c, etapa e WHERE c.dorsal=e.dorsal;

  /* Ejemplos de JOIN con ciclista y puerto */

    SELECT * FROM ciclista c NATURAL JOIN puerto p;                       -- natural join
    SELECT * FROM ciclista c JOIN puerto p ON c.dorsal = p.dorsal;        -- on
    SELECT * FROM  ciclista c INNER JOIN puerto p USING (dorsal);         -- using
    SELECT * FROM  ciclista c, puerto p WHERE c.dorsal=e.dorsal;          -- producto cartesiano

 /* Ejemplos de JOIN con equipo ciclista y etapa */

  -- Opcion 1
  SELECT *
    FROM 
    equipo e JOIN ciclista c JOIN etapa e1
    ON
     c.nomequipo=e.nomequipo AND c.dorsal=e1.dorsal;

  -- Opcion 2
  SELECT * 
    FROM
    equipo e
    JOIN ciclista c ON e.nomequipo=c.nomequipo
    JOIN etapa e1 ON c.dorsal=e1.dorsal;

  -- Opcion 3
    SELECT * FROM equipo e
      JOIN ciclista c USING(nomequipo)
      JOIN etapa e1 USING(dorsal);