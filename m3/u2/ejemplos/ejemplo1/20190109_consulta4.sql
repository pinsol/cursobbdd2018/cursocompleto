﻿/* ciclistas que han ganado alguna etapa pero NO han ganando NINGUN puerto */

                        /* SIN VISTAS */

  -- subconsulta c1 ciclistas que han ganado alguna etapa
SELECT
 DISTINCT e.dorsal
FROM etapa e
;

  -- subconsulta c2 ciclistas que han ganado puertos
SELECT
 DISTINCT p.dorsal
FROM puerto p
;

  -- subconsulta c3 ciclistas que no han ganado puertos
SELECT
 c.dorsal
FROM ciclista c
 LEFT JOIN puerto p ON c.dorsal = p.dorsal
WHERE
 p.dorsal IS NULL
;

-- consulta completa (sin vistas)
SELECT
 * 
FROM ciclistas c
JOIN etapa e ON c.dorsal = e.dorsal
JOIN puerto p ON
;


                        /* CON VISTAS */

  -- c1 ciclistas que han ganado alguna etapa
CREATE OR REPLACE VIEW consulta4c1 AS          
  SELECT
   DISTINCT e.dorsal
  FROM etapa e
;

  -- c2 ciclistas que han ganado puertos
CREATE OR REPLACE VIEW consulta4c2 AS
  SELECT
   DISTINCT p.dorsal
  FROM puerto p
;

  -- c3 ciclistas que no han ganado puertos
CREATE OR REPLACE VIEW consulta4c3 AS
  SELECT
   c.dorsal
  FROM ciclista c
   LEFT JOIN puerto p ON c.dorsal = p.dorsal
  WHERE
   p.dorsal IS NULL
;


                  /* OPERACIONES CON NUESTRAS VISTAS DE LA CONSULTA 4 */

  -- C1 - C3
-- ciclistas que han ganado etapas y puertos
  SELECT
    c1.dorsal 
  FROM
    consulta4c1 c1
    LEFT JOIN consulta4c3 c3 USING(dorsal)
  WHERE
    c3.dorsal IS NULL
    ;

  SELECT c1.dorsal FROM consulta4c1 c1 WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c3); -- USANDO NOT IN (resta)

  -- C1 - C2
-- ciclistas que han ganado etapas y no han ganado puertos                            CORRECTA
 SELECT
  c1.dorsal
 FROM
  consulta4c1 c1
  LEFT JOIN consulta4c2 c2 USING(dorsal)
 WHERE
  c2.dorsal NOT NULL
;

  -- C1 UNION C2
-- ciclistas que han ganado etapas mas los ciclistas que han ganado puertos
    SELECT * FROM consulta4c1
      UNION 
    SELECT * FROM consulta4c2
;

   -- C1 UNION C3
-- ciclistas que han ganado etapas mas ciclistas que no han ganado puertos
    SELECT * FROM  consulta4c1
      UNION
    SELECT * FROM  consulta4c3
;

    -- C1 INTERSECCION C3
-- ciclistas que han ganado etapas y ademas que no han ganado puertos                 CORRRECTA
    SELECT * FROM  consulta4c1
      INTERSECT                 -- NO EXISTE INTERSECCION EN MySQL
    SELECT * FROM  consulta4c3
;

    SELECT * FROM  consulta4c1
      NATURAL JOIN consulta4c3  -- en MySQL la interseccion se hace con NATURAL JOIN 
;