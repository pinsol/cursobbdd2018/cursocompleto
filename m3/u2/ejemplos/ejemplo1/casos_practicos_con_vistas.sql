﻿SELECT
  *
FROM
  ciclista c
  JOIN puerto p ON c.dorsal = p.dorsal 
WHERE
  p.altura>1200;


-- mejorar la consulta mediante las subconsultas
  -- c1
  SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200;

  -- consulta completa
SELECT
  c.* 
FROM (
  SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200
  ) AS c1
 JOIN ciclista c USING(dorsal);

-- vamos a utilzar vistas
  CREATE OR REPLACE VIEW consulta2c1 AS
 SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200;

  CREATE OR REPLACE VIEW consulta2 AS
    SELECT
  c.* 
FROM consulta2c1 AS c1
 JOIN ciclista c USING(dorsal);

  -- ciclistas que no han ganado etapas
SELECT
 c.* 
FROM
 ciclista c
 LEFT JOIN etapa e ON c.dorsal = e.dorsal
WHERE
e.dorsal IS NULL;

    -- realizado con subconsultas
      -- c1: ciclistas que han ganado alguna etapa
      SELECT DISTINCT e.dorsal FROM etapa e;

      -- consulta completa
      SELECT c.* FROM ciclista c LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) AS c1
        ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

      -- resta con NOT IN
        SELECT * FROM ciclista c WHERE c.dorsal NOT IN (SELECT DISTINCT e.dorsal FROM etapa e);