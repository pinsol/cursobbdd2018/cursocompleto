﻿USE practica1; -- selecciono la base de datos con la que voy a trabajar

/*
  CONSULTA 1
*/

-- COMENTARIO

SELECT
       e.emp_no,
       e.apellido,
       e.oficio,
       e.dir,
       e.fecha_alt,
       e.salario,
       e.comision,
       e.dept_no
FROM
       emple e;


/*
  CONSULTA 2
*/

SELECT
         d.dept_no,
         d.dnombre,
         d.loc
  
  FROM
  
         depart d;

/*
     CONSULTA 3
*/

SELECT DISTINCT
        e.apellido, e.oficio

FROM
        emple e;


/*
  CONSULTA 4
*/

SELECT
       d.loc,
       d.dept_no

FROM
       depart d;

/*
  CONSULTA 5
*/

  SELECT
         d.dept_no,
         d.dnombre,
         d.loc
  
  FROM
         depart d;

  /*
  CONSULTA 6
*/

    SELECT 
    COUNT(e.emp_no) TotalEmpleados 
    
    FROM 
    emple e;

