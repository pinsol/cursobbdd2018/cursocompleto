﻿USE ciclistas;

SELECT * FROM ciclista;
SELECT * FROM equipo;
SELECT * FROM etapa;
SELECT * FROM lleva;
SELECT * FROM maillot;
SELECT * FROM puerto;



/* (d) Obtener los datos de las etapas que no comienzan en la misma ciudad
       en que acaba la etapa anterior */
SELECT
  e1.* 
FROM
  etapa e1 JOIN etapa e2 ON e1.numetapa-1=e2.numetapa
AND
 e2.llegada<>e1.salida
;

/* (a) Obtener los datos de las etapas que pasan por algún puerto de montaña
       y que tienen salida y llegada en la misma población */
SELECT
 * 
FROM
 (SELECT DISTINCT numetapa FROM puerto) c1 JOIN etapa USING(numetapa) 
WHERE
 salida=llegada
;

/* (03) Cuántos puertos ha ganado Induráin llevando algún maillot */
SELECT
  COUNT(*) 
FROM (
  SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin') c1
    JOIN puerto USING(dorsal)
    JOIN lleva USING(dorsal, numetapa)
;

/* (c) Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa 
       llevando el maillot amarillo, mostrando también el número de etapa */
SELECT código FROM maillot WHERE color='amarillo';

SELECT
  nombre, nomequipo, etapa.numetapa 
FROM(
  SELECT código FROM maillot WHERE color='amarillo'
  )c1 JOIN lleva USING(código)
  JOIN etapa ON etapa.dorsal=lleva.dorsal AND etapa.numetapa=lleva.numetapa
  JOIN ciclista ON etapa.dorsal=ciclista.dorsal
;

/* (g) Obtener el nombre y el equipo de los ciclistas que han llevado algún maillot
       o que han ganado algún puerto */
SELECT DISTINCT nombre,nomequipo FROM lleva JOIN ciclista USING(dorsal)
  UNION
SELECT DISTINCT nombre,nomequipo FROM puerto JOIN ciclista USING(dorsal)
;

/* (02) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot */
SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';
SELECT DISTINCT numetapa FROM lleva WHERE dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin');
SELECT DISTINCT etapa.numetapa FROM etapa JOIN puerto USING(numetapa);

SELECT COUNT(*) FROM(
SELECT numetapa,dorsal FROM puerto WHERE dorsal=(
SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin'
)
)c2 JOIN lleva USING(numetapa,dorsal)
;

/* (07) Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado */
SELECT
 DISTINCT código 
FROM
 lleva
 JOIN (SELECT etapa.numetapa FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa)c1
WHERE
 dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin')
;

/* (09) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot */
SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';

SELECT * FROM etapa JOIN puerto USING(numetapa);

SELECT
  COUNT(DISTINCT lleva.numetapa) 
FROM lleva
   JOIN (SELECT etapa.numetapa FROM etapa JOIN puerto USING(numetapa))c1 USING(numetapa) 
WHERE
 lleva.dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin')
;

SELECT COUNT(*) FROM(
SELECT c2.numetapa FROM(
SELECT DISTINCT dorsal,numetapa FROM(
SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin'
)c1 JOIN lleva USING(dorsal)
)c2 JOIN etapa ON c2.dorsal=etapa.dorsal AND c2.numetapa=etapa.numetapa
)C3 JOIN puerto USING(numetapa)
;

/* (n) Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura */
SELECT
 c.dorsal,c.nombre 
FROM
 ciclista c
   JOIN (SELECT dorsal FROM puerto WHERE altura=(SELECT MAX(altura) FROM puerto))c1 USING(dorsal)
;

/* (m) Obtener las poblaciones de salida y de llegada de las etapas
       donde se encuentran los puertos con mayor pendiente */
SELECT salida,llegada FROM etapa WHERE numetapa=(
SELECT numetapa FROM puerto WHERE pendiente=(
SELECT MAX(pendiente) FROM puerto))
;

SELECT numetapa FROM puerto WHERE pendiente=(
SELECT MAX(pendiente) FROM puerto);


/* (h) Obtener los datos de los ciclistas que han vestido todos los maillots
       (no necesariamente en la misma etapa) */
SELECT COUNT(*) n FROM maillot m;
SELECT DISTINCT dorsal,código FROM lleva;

SELECT
 * 
FROM(
SELECT dorsal FROM(
SELECT DISTINCT dorsal,código FROM lleva
)c1 GROUP BY dorsal HAVING COUNT(*)=(
SELECT COUNT(*) FROM maillot)
  )c4
  JOIN ciclista USING(dorsal)
;

/* (p) Obtener el nombre de los ciclistas, ordenados alfabéticamente,
       que pertenecen a un equipo de más de cinco ciclistas y que han ganado alguna etapa,
       indicando también cuántas etapas han ganado */
SELECT c.nomequipo,COUNT(c.nombre) n FROM ciclista c GROUP BY c.nomequipo;

SELECT c.nomequipo,COUNT(c.nombre) n FROM ciclista c GROUP BY c.nomequipo HAVING n>5;

SELECT c1.nomequipo,c.nombre,c.dorsal FROM ciclista c JOIN (
  SELECT c.nomequipo,COUNT(c.nombre) n FROM ciclista c GROUP BY c.nomequipo HAVING n>5
  ) c1 ON c.nomequipo=c1.nomequipo;

SELECT * FROM etapa e JOIN (
  SELECT c.dorsal,c1.nomequipo,c.nombre FROM ciclista c JOIN (
  SELECT c.nomequipo,COUNT(c.nombre) n FROM ciclista c GROUP BY c.nomequipo HAVING n>5
  ) c1 ON c.nomequipo=c1.nomequipo
  )c2 ON e.dorsal=c2.dorsal;

SELECT
  c.nombre,COUNT(numetapa)
FROM
  ciclista c
    JOIN etapa e ON c.dorsal = e.dorsal
    JOIN (
  SELECT c.nomequipo,COUNT(c.nombre) n FROM ciclista c GROUP BY c.nomequipo HAVING n>5
)c1 ON c.nomequipo=c1.nomequipo GROUP BY c.nombre ORDER BY c.nombre;