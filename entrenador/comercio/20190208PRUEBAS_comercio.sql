﻿USE comercios;

/* (60) Nombre de los comercios que distribuyen únicamente 2 programas.
Entendiendo por un único programa a todas sus versiones */
SELECT
 DISTINCT nombre 
FROM
  (SELECT
     cif
   FROM(
      SELECT 
        DISTINCT cif,nombre 
      FROM
        programa JOIN distribuye USING(codigo)
      )c1
  GROUP BY cif HAVING COUNT(*)=2
  )c2 
  JOIN comercio USING(cif)
;

/* (64) Ciudades donde se puedan adquirir programas distribuidos en exclusiva por los comercios
  (no los locales comerciales, aunque sí que hay que tener en cuenta el stock) */
SELECT DISTINCT ciudad FROM(
SELECT programa FROM(
SELECT DISTINCT programa.nombre programa,comercio.nombre comercio
FROM programa JOIN distribuye USING(codigo)
JOIN comercio USING(cif)
)c1 GROUP BY 1 HAVING COUNT(*)=1
)c2 JOIN programa ON programa=nombre
JOIN distribuye USING(codigo)
JOIN comercio USING(cif)
;

/* (57) Nombre de los comercios que sólo distribuyen un único programa además de Windows */
SELECT nombre FROM(
SELECT cif FROM distribuye JOIN(
SELECT codigo FROM programa WHERE nombre='Windows'
)c1 USING(codigo)
)c3 JOIN (SELECT cif FROM(
SELECT DISTINCT cif,nombre FROM programa JOIN distribuye USING(codigo)
)c2 GROUP BY 1 HAVING COUNT(*)=2
)c4 USING(cif)
JOIN comercio USING(cif)
;

/* (63) Nombres de comercios (no locales comerciales) que tengan exclusivas de distribución.
  Es decir, que distribuyan programas que sólo ellos distribuyen */
SELECT DISTINCT comercio FROM(
SELECT DISTINCT programa.nombre programa,comercio.nombre comercio FROM programa JOIN distribuye USING(codigo)
JOIN comercio USING(cif)
)c1 JOIN(
SELECT programa FROM(
SELECT DISTINCT programa.nombre programa,comercio.nombre comercio
FROM programa JOIN distribuye USING(codigo)
JOIN comercio USING(cif)
)c1 GROUP BY 1 HAVING COUNT(*)=1
)c2 USING(programa);