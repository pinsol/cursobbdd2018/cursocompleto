﻿/* 41. Nombre de aquellos fabricantes cuyo país es el mismo que Oracle (Subconsulta) */
  SELECT f.id_fab,f.pais FROM fabricante f WHERE f.nombre LIKE 'Oracle'; -- pais de Oracle
  
  SELECT
   f.nombre 
  FROM
   (SELECT id_fab,pais FROM fabricante WHERE nombre='Oracle') c1
   JOIN fabricante f ON c1.pais=f.pais 
    AND c1.id_fab<>f.id_fab
  ;

/* 42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez (Subconsulta), excluyendo Pepe Pérez */
SELECT
   cliente.nombre 
  FROM
   (SELECT dni,edad FROM cliente WHERE nombre='Pepe Pérez') c1
   JOIN cliente ON c1.edad=cliente.edad 
    AND c1.dni<>cliente.dni
;

/* 43. Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio FNAC (Subconsulta), excluyendo a la FNAC */
SELECT
   comercio.nombre
  FROM
   (SELECT ciudad,cif FROM comercio WHERE nombre='FNAC') c1
   JOIN comercio ON c1.ciudad=comercio.ciudad
    AND c1.cif<>comercio.cif
;

/* 27. Genera un listado de los programas que desarrolla Oracle */
SELECT
 p.*
FROM
  fabricante f
  JOIN desarrolla d ON f.id_fab = d.id_fab
  JOIN programa p ON d.codigo = p.codigo
WHERE
  f.nombre LIKE 'Oracle'
;

/* 30. ¿Qué fabricante ha desarrollado Freddy Hardest? */
SELECT fabricante.nombre
FROM fabricante
 JOIN desarrolla USING(id_fab)
 JOIN (SELECT codigo FROM programa WHERE nombre='Freddy Hardest') c1 USING(codigo)
;


/* 31. Selecciona el nombre de los programas que se registran por Internet */
SELECT
 nombre 
FROM
 programa
 JOIN (SELECT * FROM registra WHERE medio LIKE 'Internet') c1 USING(codigo)
;

/* 32. Selecciona el nombre de las personas que se registran por Internet */
SELECT
 nombre 
FROM
 cliente
 JOIN registra USING(dni)
WHERE
 medio='Internet'
  ORDER BY cliente.dni
;
