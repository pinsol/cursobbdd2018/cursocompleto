﻿USE comercios;

select comercio.* from
(select codigo from programa where nombre='windows') c1 Join
distribuye using(codigo)
right join comercio using(cif) where c1.codigo is null;

SELECT * FROM programa p WHERE p.nombre='Windows';
SELECT * FROM distribuye d JOIN (SELECT * FROM programa p WHERE p.nombre='Windows') c1 USING(codigo);
SELECT comercio.* FROM comercio c LEFT JOIN (SELECT d.cif FROM distribuye d JOIN (SELECT p.codigo FROM programa p WHERE p.nombre='Windows') c1 USING(codigo);
) c2 USING(cif);

select distinct nombre,version from(
select codigo, count(*) n from distribuye
group by codigo
having n=1
)c1 join programa using(codigo);

SELECT p.codigo,p.nombre,p.version FROM (
SELECT p.nombre, COUNT(*) n FROM programa p GROUP BY p.nombre HAVING n>1
  ) c1
  JOIN programa p USING(nombre) ORDER BY p.nombre,p.version DESC;

SELECT c1.nombre FROM (
SELECT p.nombre,COUNT(*) n FROM programa p GROUP BY p.nombre)
  c1 WHERE n=1;

SELECT p.nombre,COUNT(*) FROM programa p GROUP BY p.nombre;

SELECT
  *
FROM distribuye d
  JOIN programa p USING(codigo)
  JOIN comercio c USING(cif)
;

SELECT programa FROM(
SELECT DISTINCT p.nombre programa,c.nombre comercio
FROM programa p JOIN distribuye USING(codigo) JOIN comercio c USING(cif)
)c1  GROUP BY 1 HAVING COUNT(*)=1;