﻿/* 33. ¿Qué medios ha utilizado para registrarse Pepe Pérez? */
SELECT medio 
FROM registra 
  JOIN 
  (SELECT dni FROM cliente WHERE nombre='Pepe Pérez') c1 USING(dni)
;

/* 35. ¿Qué programas han recibido registros por tarjeta postal? */
SELECT
  nombre 
FROM
  (SELECT codigo FROM registra WHERE medio='tarjeta postal') c1
    JOIN programa USING(codigo)
;

/* 36. ¿En qué localidades se han registrado productos por Internet? */
SELECT
  DISTINCT ciudad
FROM 
  (SELECT cif FROM registra WHERE medio='Internet') c1
    JOIN comercio USING(cif)
;

/* 37. Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro */
SELECT
  cliente.nombre,programa.nombre 
FROM 
  (SELECT dni,codigo FROM registra WHERE medio='Internet') c1
    JOIN cliente USING(dni)
    JOIN programa USING(codigo)
;

/* 40. Obtén el nombre de los usuarios que han registrado Access XP */
SELECT
 nombre 
FROM
 (SELECT codigo FROM programa WHERE nombre='Access' AND version='XP') c1
  JOIN registra USING(codigo)
  JOIN cliente USING(dni)
;

/* 28. ¿Qué comercios distribuyen Windows? */
SELECT
 DISTINCT COMERCIO.* 
FROM
 (SELECT codigo FROM programa WHERE nombre='Windows') c1
  JOIN distribuye d USING(codigo)
  JOIN comercio USING(cif)
;

/* 29. Genera un listado del nombre de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid */
SELECT
  * 
FROM 
  (SELECT codigo,cantidad FROM
  (SELECT cif FROM comercio WHERE nombre='El Corte Inglés' AND ciudad='Madrid') c1
  JOIN distribuye USING(cif)) c2
  JOIN programa USING(codigo)
;

/* 39. Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle */
  SELECT DISTINCT ciudad FROM 
  (SELECT id_fab FROM fabricante WHERE nombre='Oracle') c1
  JOIN desarrolla USING(id_fab)
  JOIN distribuye USING(codigo)
  JOIN comercio USING(cif)
  ;

  /* 53. Obtener el número total de programas que se han distribuido en Sevilla */
SELECT SUM(cantidad) FROM 
(SELECT cif FROM comercio WHERE ciudad='Sevilla') c1
JOIN distribuye USING(cif);

/* 54. Calcular el número total de programas que han desarrollado los fabricantes cuyo país es Estados Unidos */
SELECT COUNT(*) FROM 
(SELECT id_fab FROM fabricante WHERE pais='Estados Unidos') c1
JOIN desarrolla USING(id_fab);

