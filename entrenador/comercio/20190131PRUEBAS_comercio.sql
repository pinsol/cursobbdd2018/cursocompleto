﻿USE comercios;

/* 28(d).- ¿Qué comercios sólo distribuyen Windows? */
SELECT
 *
FROM
 (SELECT DISTINCT cif FROM (SELECT codigo FROM programa WHERE nombre='windows') c1 JOIN distribuye USING(codigo)) c2
WHERE cif NOT IN(SELECT DISTINCT cif FROM(SELECT codigo FROM programa WHERE nombre!='windows') c3
 JOIN distribuye USING(codigo)
;

/* 28(f).- ¿Qué comercios sólo distribuyen un programa? */
SELECT
  * 
FROM
  programa
  JOIN distribuye USING(codigo)
  JOIN comercio USING(cif)
  ;

/* 44.- Nombre de aquellos clientes, ordenados alfabéticamente, que han registrado un producto de la misma forma que el cliente Pepe Pérez (Subconsulta) */
SELECT dni FROM cliente WHERE nombre='Pepe Pérez'; -- dni de Pepe Pérez

CREATE OR REPLACE VIEW 44c1 AS
SELECT medio,dni FROM registra WHERE dni=(SELECT dni FROM cliente WHERE nombre='Pepe Pérez'); -- medios de 

SELECT nombre FROM cliente c
JOIN 44c1 USING(dni)
ORDER BY c.nombre
;

/* 28b.- ¿Qué comercios NO distribuyen Windows? */
SELECT * FROM 

/* Consultas ciclistas */
USE ciclistas;

/* I.- Obtener el nombre de los puertos de montaña que tienen una altura superior a la altura media de todos los puertos */
SELECT AVG(p.altura) FROM puerto p;

SELECT
 nompuerto 
FROM
 puerto 
WHERE
 altura>(SELECT AVG(altura) FROM puerto)
;

/* 8.- ¿Cuántos maillots diferentes ha llevado Induráin? */
SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';

SELECT
  COUNT(DISTINCT código) 
FROM
 lleva 
WHERE
 dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin')
;

/* e.- Obtener el número de las etapas que tienen algún puerto de montaña, indicando cuántos tiene cada una de ellas */
SELECT
 etapa.numetapa ,COUNT(nompuerto)
FROM
 etapa 
 JOIN puerto USING(numetapa)
GROUP BY 
 etapa.numetapa
;

/* k.- Obtener la edad media de los ciclistas que han ganado alguna etapa */
SELECT
 AVG(edad) 
FROM
  ciclista
 JOIN (SELECT DISTINCT dorsal FROM etapa) c1 USING(dorsal)
;




/* EJEMPLO DIVISION ciclistas */
SELECT COUNT(*) FROM maillot;

SELECT dorsal,COUNT(DISTINCT código) FROM lleva GROUP BY dorsal;

SELECT COUNT(DISTINCT código) FROM lleva 
;

SELECT 
lleva.dorsal,COUNT(*) n
FROM
 (SELECT dorsal,COUNT(DISTINCT código) FROM lleva GROUP BY dorsal) c1
GROUP BY dorsal
 HAVING n=(SELECT COUNT(DISTINCT código) FROM lleva)
;

SELECT 
dorsal,COUNT(*) n
FROM
 lleva
GROUP BY dorsal
 HAVING n=(SELECT COUNT(DISTINCT código) FROM lleva)
;