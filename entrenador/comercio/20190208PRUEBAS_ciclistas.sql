﻿USE ciclistas;

/* (b) Obtener las poblaciones que tienen la meta de alguna etapa,
  pero desde las que no se realiza ninguna salida. Resuélvela utilizando NOT IN */
SELECT
 DISTINCT llegada
FROM
 etapa
WHERE
  llegada NOT IN (SELECT DISTINCT salida FROM etapa)
;

USE jardineria;

/* (4.07.14) Sacar los distintos estados por los que puede pasar un pedido */
SELECT DISTINCT Estado FROM pedidos;

/* (4.10.41) ¿De qué ciudades son los clientes? */
SELECT DISTINCT Ciudad FROM clientes;

