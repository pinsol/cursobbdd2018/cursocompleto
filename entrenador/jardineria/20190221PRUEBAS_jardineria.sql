﻿USE jardineria;

/* (4.09.02) Sacar el nombre de los clientes que no hayan hecho pagos, junto al nombre de sus representantes
             (sin apellidos) y la ciudad de la oficina la que pertenece el representante */
SELECT * FROM pagos;
SELECT * FROM pedidos;

SELECT DISTINCT c.CodigoCliente FROM clientes c LEFT JOIN pagos ON c.CodigoCliente = pagos.CodigoCliente
WHERE pagos.CodigoCliente IS NULL
;


SELECT nombrecliente,nombre,ciudad FROM (SELECT DISTINCT nombrecliente,CodigoEmpleadoRepVentas codigoempleado FROM clientes LEFT JOIN(
  SELECT DISTINCT codigocliente FROM pagos) c1
  USING(CodigoCliente)WHERE c1.CodigoCliente IS NULL)c2 JOIN empleados
  USING(CodigoEmpleado) JOIN oficinas USING(CodigoOficina);

/* (4.10.40) Códigos de los frutales no vendidos */
SELECT * FROM productos;

SELECT DISTINCT CodigoProducto FROM productos WHERE gama='Frutales'; -- codigos FR

SELECT * FROM (SELECT CodigoProducto FROM productos WHERE gama='frutales') c1 LEFT JOIN
(SELECT DISTINCT codigoProducto FROM(SELECT CodigoProducto FROM productos WHERE gama='frutales') c1
JOIN detallepedidos USING(CodigoProducto)) c2 USING(codigoProducto) WHERE c2.CodigoProducto IS NULL;

/* (4.08.09) ¿Qué cliente(s) ha rechazado más pedidos? */
SELECT Codigocliente, COUNT(*) n FROM pedidos WHERE Estado='rechazado' GROUP BY CodigoCliente HAVING n=2 JOIN clientes USING (codigocliente);

  SELECT nombrecliente FROM (SELECT CodigoCliente,COUNT(*) n FROM pedidos WHERE Estado='rechazado' GROUP BY CodigoCliente HAVING n=2) c1 JOIN clientes USING(codigocliente);

/* (03) Obtener el nombre de los países desde donde no se han realizado pedidos */
SELECT
  DISTINCT pais 
FROM
  clientes c 
WHERE
  pais NOT IN
(SELECT DISTINCT c.Pais FROM clientes c JOIN pedidos p ON c.CodigoCliente = p.CodigoCliente)
;

/* (4.10.33) Código de las oficinas que no venden frutales */
SELECT * FROM oficinas;

SELECT DISTINCT e.CodigoOficina FROM empleados e JOIN
(SELECT codigoempleadorepventas FROM clientes c WHERE c.CodigoCliente not IN
(SELECT CodigoCliente FROM pedidos WHERE CodigoPedido NOT IN
(SELECT codigopedido FROM detallepedidos d WHERE d.CodigoProducto NOT IN
(SELECT CodigoProducto FROM productos WHERE gama LIKE 'frutales'))))c1 ON e.CodigoEmpleado = c1.CodigoEmpleadoRepVentas;