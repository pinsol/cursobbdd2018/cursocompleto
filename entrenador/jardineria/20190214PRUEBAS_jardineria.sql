﻿USE jardineria;

/* (4.07.11) Clientes que no tiene asignado representante de ventas  */
SELECT
 * 
FROM
 clientes 
WHERE
 CodigoEmpleadoRepVentas IS NULL
;

/* (4.07.06) Sacar el nombre de los clientes españoles  */
SELECT
 NombreCliente 
FROM
 clientes 
WHERE
 Pais='España' OR Pais='Spain'
;

/* (4.09.03) Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes */
SELECT
 e.nombre, e2.nombre 
FROM
 empleados e
 JOIN empleados e2 ON e.CodigoJefe = e2.CodigoEmpleado
;

/* (4.08.01) Obtener el nombre del producto más caro */
SELECT MAX(p.PrecioVenta) FROM productos p;

SELECT
 Nombre 
FROM
 productos 
WHERE
  PrecioVenta=(SELECT MAX(p.PrecioVenta) FROM productos p)
;
    
/* (4.07.07) Sacar cuántos clientes tiene cada país */
SELECT
 pais, COUNT(*) 
FROM
  clientes 
  GROUP BY
   Pais
;

/* (4.07.10) Sacar el código de empleado y número de clientes al que atiende cada representante de ventas */
SELECT
 c.CodigoEmpleadoRepVentas, COUNT(*) 
FROM
 clientes 
GROUP BY 
 CodigoEmpleadoRepVentas
;

/* (4.10.11) Sacar los clientes que residan en la misma ciudad donde hay una oficina, indicando dónde está la oficina */
SELECT
 o.Ciudad,c.NombreCliente,o.LineaDireccion1,o.LineaDireccion2 
FROM
  clientes c 
   JOIN oficinas o ON c.Ciudad = o.Ciudad
;

/* (4.10.17) Sacar cuántos pedidos tiene cada cliente en cada estado */
SELECT
 CodigoCliente,Estado,COUNT(*) 
FROM
 pedidos 
   GROUP BY CodigoCliente,Estado
;

/* (4.09.04) Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido.
             Es decir, FechaEntrega mayor que FechaEsperada */
SELECT DISTINCT p.CodigoCliente FROM pedidos p WHERE p.FechaEntrega>p.FechaEsperada;

SELECT
 c.NombreCliente
FROM
  clientes c
    JOIN (SELECT DISTINCT p.CodigoCliente FROM pedidos p WHERE p.FechaEntrega>p.FechaEsperada) c1 USING(CodigoCliente)
;

/* (4.10.36) Código del frutal que más margen de beneficios proporciona */
SELECT
 CodigoProducto,PrecioVenta-PrecioProveedor 
FROM
 productos 
WHERE
 Gama='Frutales'
;

/* (4.10.03) Sacar el nombre de los clientes que hayan hecho pedidos en 2008 */
SELECT p.CodigoCliente,year(p.FechaPedido) FROM pedidos p;

SELECT
 c.NombreCliente
FROM
 clientes c 
  JOIN (SELECT DISTINCT p.CodigoCliente FROM pedidos p WHERE year(p.FechaPedido)=2008)c1
     ON c.CodigoCliente=c1.CodigoCliente
;

/* (4.08.04) Sacar el códido de el/los producto/s que más unidades tiene en stock y el que menos */
SELECT
  CodigoProducto 
FROM
  productos 
WHERE CantidadEnStock=(
        SELECT MAX(CantidadEnStock) FROM productos)
    UNION
SELECT
  CodigoProducto 
FROM
  productos 
WHERE CantidadEnStock=(
        SELECT min(CantidadEnStock) FROM productos)
;

/* (4.10.02) Sacar el listado con los nombres de los clientes y el total pagado por cada uno de ellos */
SELECT
  NombreCliente,Pagos
FROM
  (SELECT
    CodigoCliente,SUM(Cantidad) Pagos
   FROM
    pagos
    GROUP BY CodigoCliente)c1
  JOIN clientes USING(CodigoCliente)
;

/* (4.10.13) Sacar el número de clientes que tiene asignado cada representante de ventas mostrando su nombre */
SELECT Nombre,n FROM 
(SELECT
 CodigoEmpleadoRepVentas CodigoEmpleado,COUNT(*) n 
FROM
 clientes
 GROUP BY CodigoEmpleadoRepVentas)c1
JOIN empleados USING(CodigoEmpleado)
;

USE nba;

/* Equipos y ciudad de los jugadores españoles de la NBA */
SELECT
 Nombre,ciudad 
FROM
 (SELECT
   DISTINCT Nombre_equipo
  FROM jugadores
  WHERE Procedencia='Spain')c1
JOIN equipos ON c1.Nombre_equipo=Nombre
;