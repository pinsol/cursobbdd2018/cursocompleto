﻿USE jardineria;

/* (4.10.09) Sacar el nombre, apellidos, oficina (ciudad) y cargo del empleado que no represente a ningún cliente */
SELECT
 e.Nombre,e.Apellido1,e.Apellido2,o.Ciudad,e.Puesto 
FROM
 empleados e
  JOIN oficinas o USING(CodigoOficina)
  LEFT JOIN clientes c ON e.CodigoEmpleado = c.CodigoEmpleadoRepVentas
    WHERE c.CodigoEmpleadoRepVentas IS null
;


/* (4.08.03) Obtener los nombres de los clientes cuya línea de crédito sea mayor que los pagos que haya realizado */
SELECT NombreCliente FROM(
SELECT CodigoCliente, SUM(Cantidad) total
FROM pagos Group by codigocliente)
c1 join clientes using (codigocliente) where limitecredito>total;
