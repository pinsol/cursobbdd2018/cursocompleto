﻿USE emcan;

SELECT * FROM centros;
SELECT * FROM cps;
SELECT * FROM cursos;
SELECT * FROM familias;
SELECT * FROM inicios;
SELECT * FROM programacion;



/* (07) ¿Qué centros impartirán la mayor cantidad de cursos de informática en Santander? */
SELECT
 centro, COUNT(codigo) 
FROM programacion
 JOIN centros c ON programacion.cod_centro = c.cod_centro 
WHERE c.municipio='Santander'
 AND codigo LIKE '%IFC%'
 GROUP BY c.cod_centro
;

SELECT centro FROM(
SELECT cod_centro FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%'
)c1 JOIN programacion ON fml=SUBSTR(codigo,1,3)
JOIN(
SELECT cod_centro FROM centros
WHERE municipio='santander'
)c2 USING(cod_centro)
GROUP BY 1 HAVING COUNT(*)=(
SELECT MAX(n) FROM(
SELECT cod_centro,COUNT(*) n FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%'
)c1 JOIN programacion ON fml=SUBSTR(codigo,1,3)
JOIN(
SELECT cod_centro FROM centros
WHERE municipio='santander'
)c2 USING(cod_centro)
GROUP BY 1
)c3
)
)c4 JOIN centros USING(cod_centro);

/* (08) ¿Qué centro impartirá la mayor cantidad de cursos de informática de nivel 3 en Cantabria? */
SELECT
  c1.centro,p.cod_centro,COUNT(p.codigo) 
FROM
  programacion p
  JOIN cursos c ON p.codigo = c.codigo
  JOIN centros c1 ON p.cod_centro = c1.cod_centro
WHERE
  c.nivel=3 
  GROUP BY cod_centro
;

SELECT centro FROM(
SELECT cod_centro FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%'
)c1 JOIN cursos
ON fml=SUBSTRE(codigo,1,3) AND nivel=3
JOIN programacion USING(codigo)
GROUP BY 1 HAVING COUNT(*)=(
SELECT MAX(n) FROM(
SELECT cod_centro,COUNT(*) n FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%'
)c1 JOIN cursos
ON fml=SUBSTR(codigo,1,3) AND nivel=3
JOIN programacion USING(codigo)
GROUP BY 1
)c2
)
)c3 JOIN centros USING(cod_centro);

/* (11) ¿Cuál es el curso de informática que más se impartirá? */
SELECT * FROM programacion;
SELECT fml FROM familias WHERE familia LIKE '%informátic%';
SELECT codigo,COUNT(*) n FROM programacion GROUP BY codigo HAVING codigo LIKE '%ifc%';
SELECT MAX(n) FROM (SELECT codigo,COUNT(*) n FROM programacion GROUP BY codigo HAVING codigo LIKE '%ifc%')c1;
SELECT especialidad FROM (SELECT codigo,MAX(n) FROM (SELECT codigo,COUNT(*) n FROM programacion GROUP BY codigo HAVING codigo LIKE '%ifc%')c1)c2 JOIN cursos ON c2.codigo=cursos.codigo;

/* (15) ¿En qué familia profesional está especializado cada centro?
        Indíquese centro, municipio y familia profesional;
        y ordénese por municipio y especialidad */
SELECT * FROM centros c;
SELECT * FROM programacion p;
SELECT * FROM familias;


SELECT DISTINCT p.cod_centro FROM programacion p;
SELECT SUBSTR(codigo,1,3) curso FROM programacion p;

SELECT DISTINCT c.centro,c.municipio,familia FROM programacion p
  JOIN familias ON fml=SUBSTR(p.codigo,1,3)
  JOIN centros c ON p.cod_centro = c.cod_centro
ORDER BY c.municipio,familia;