﻿USE emcan;

SELECT * FROM centros;
SELECT * FROM cps;
SELECT * FROM cursos;
SELECT * FROM familias;
SELECT * FROM inicios;
SELECT * FROM programacion;



/* (01) ¿Cuántos centros hay censados en Cantabria? */
SELECT COUNT(*) n FROM centros;

/* (02) ¿Cuántos cursos se han programado? */
SELECT COUNT(*) FROM cursos;

/* (03) ¿Cuántos centros han resultado adjudicatarios de estos cursos? */
SELECT cod_centro,COUNT(*) n FROM programacion GROUP BY cod_centro;

/* (09) ¿Cuántos cursos de informática se imparten en Cantabria? */
SELECT COUNT(*) FROM programacion WHERE codigo LIKE '%IFC%';

/* (10) ¿Cuántos cursos de informática diferentes se imparten en Cantabria?  */
SELECT DISTINCT codigo FROM programacion WHERE codigo LIKE '%IFC%';

/* (12) ¿Cuántos cursos de informática de nivel 3 se imparten en Cantabria? */
SELECT codigo FROM cursos WHERE nivel=3 AND codigo LIKE '%IFC%';

SELECT * FROM programacion JOIN (SELECT codigo FROM cursos WHERE nivel=3 AND codigo LIKE '%IFC%'
)c1 USING(codigo);

/* (12) ¿Cuántos cursos de informática de nivel 3 diferentes se imparten en Cantabria? */
SELECT COUNT(DISTINCT c1.codigo) FROM programacion JOIN (SELECT codigo FROM cursos WHERE nivel=3 AND codigo LIKE '%IFC%')c1 USING(codigo);

/* (17) ¿En cuántos municipios se imparten cursos? */
SELECT municipio, COUNT(*) FROM programacion 
  JOIN centros c ON programacion.cod_centro = c.cod_centro GROUP BY c.municipio;

/* (18) En qué municipio hay más centros de formación */
SELECT municipio,COUNT(*) FROM centros GROUP BY municipio;

/* (19) Si acabas de cursar mate y lengua N2 o tienes la ESO,
        ¿dónde tendrías que hacer un curso en Santander para poder cursar en Alpe Aplicaciones Web?
        Indica curso, centro, dirección postal y fecha de inicio, ordenando el listado por ésta última.
        Recuerda que un certificado de nivel 2 da acceso a un nivel 3  de su misma familia profesional y que,
        para cursar un nivel 2 necesitas la ESO y un nivel 3 Bachiller,
        o un ciclo formativo grado medio o superior en formación profesional, respectivamente */
SELECT * FROM centros WHERE c.municipio='Santander';
SELECT * FROM cursos WHERE nivel=2;



SELECT especialidad, centro, direccion, inicio FROM programacion JOIN (
SELECT codigo,especialidad FROM cursos
WHERE nivel=2 AND SUBSTR(codigo,1,3)=(
SELECT DISTINCT SUBSTR(codigo,1,3)
FROM cursos WHERE especialidad
LIKE '%aplicaciones%web%'
)
) cursos USING(codigo) JOIN(
SELECT * FROM centros
WHERE municipio='santander'
) centros USING(cod_centro)
ORDER BY inicio;

SELECT familia FROM(
SELECT SUBSTR(codigo,1,3) fml
FROM programacion GROUP BY 1 HAVING COUNT(*)=(
SELECT MAX(n) FROM(
SELECT SUBSTR(codigo,1,3),
COUNT(*) n
FROM programacion GROUP BY 1) c1))c2 JOIN familias USING(fml);

/* (05) ¿Qué centro impartirá la mayor cantidad de cursos en Santander? */
SELECT * 
FROM programacion p 
  JOIN centros c ON p.cod_centro = c.cod_centro
  GROUP BY c.centro;


/* (06) ¿Qué centro impartirá la mayor cantidad de cursos de informática en Cantabria? */
SELECT centro FROM(
SELECT cod_centro FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%'
)c1 JOIN programacion ON fml=SUBSTR(codigo,1,3)
GROUP BY 1 HAVING COUNT(*)=(
SELECT MAX(n) FROM(
SELECT cod_centro,COUNT(*)n FROM(
SELECT fml FROM familias
WHERE familia LIKE '%informática%')c1
JOIN programacion ON fml=SUBSTR(codigo,1,3) GROUP BY 1)c2)c3
JOIN centros USING(cod_centro);