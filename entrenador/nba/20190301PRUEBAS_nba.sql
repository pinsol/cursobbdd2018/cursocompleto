﻿USE nba;

SELECT * FROM equipos;
SELECT * FROM estadisticas;
SELECT * FROM jugadores;
SELECT * FROM partidos;



/* (14) Equipo que ha ganado más partidos */
SELECT ganador FROM(
SELECT equipo_local ganador FROM partidos WHERE puntos_local>puntos_visitante UNION ALL
SELECT equipo_visitante ganador FROM partidos WHERE puntos_local
)c1 GROUP BY 1 HAVING COUNT(*)=(
SELECT MAX(n) FROM(
SELECT ganador,COUNT(*) n FROM(
SELECT equipo_local ganador FROM partidos WHERE puntos_local>puntos_visitante UNION ALL
SELECT equipo_visitante ganador FROM partidos WHERE puntos_local)c1 GROUP BY 1)c2);


SELECT ganador FROM (
    SELECT equipo_local ganador FROM partidos WHERE puntos_local>puntos_visitante
      UNION ALL
    SELECT equipo_visitante ganador FROM partidos WHERE puntos_local<puntos_visitante  
  ) c1 GROUP BY  1 HAVING COUNT(*)=(
    SELECT MAX(n) FROM (
        SELECT ganador,COUNT(*) n FROM (
            SELECT equipo_local ganador FROM partidos WHERE puntos_local>puntos_visitante
              UNION ALL
            SELECT equipo_visitante ganador FROM partidos WHERE puntos_local<puntos_visitante  
          ) c1 GROUP BY  1  
      ) c2  
  );

/* (17) Nombre de los equipos que anotan en total más puntos jugando fuera que en casa,
ordenados alfabéticamente */
SELECT * FROM partidos;

SELECT equipo_local equipo,SUM(puntos_local) ptosLocal FROM partidos GROUP BY equipo_local;
SELECT equipo_visitante equipo,SUM(puntos_visitante) ptosVisi FROM partidos GROUP BY equipo_local;


SELECT equipo FROM (SELECT equipo_local equipo,SUM(puntos_local) ptosLocal FROM partidos GROUP BY equipo_local)c1 
  JOIN (SELECT equipo_visitante equipo,SUM(puntos_visitante) ptosVisi FROM partidos GROUP BY equipo_visitante
)c2
USING(equipo) WHERE c2.ptosVisi>c1.ptosLocal;

/* (13) ¿Qué equipo tiene a los jugadores más ligeros? */
SELECT Nombre_equipo,SUM(peso) FROM jugadores GROUP BY Nombre_equipo;

/* (16) Temporada en la que se anotaron más puntos por la totalidad de los equipos */
SELECT DISTINCT temporada FROM partidos;
SELECT temporada,equipo_local equipo,SUM(puntos_local) ptosLocal FROM partidos GROUP BY equipo_local,temporada;
SELECT temporada,equipo_visitante equipo,SUM(puntos_visitante) ptosVisi FROM partidos GROUP BY equipo_local,temporada;

SELECT c1.temporada,c1.ptosLocal+c2.ptosVisi FROM (SELECT temporada,equipo_local equipo,SUM(puntos_local) ptosLocal FROM partidos GROUP BY equipo_local,temporada)c1 
  JOIN (SELECT temporada,equipo_visitante equipo,SUM(puntos_visitante) ptosVisi FROM partidos GROUP BY equipo_visitante,temporada
)c2
USING(equipo);
