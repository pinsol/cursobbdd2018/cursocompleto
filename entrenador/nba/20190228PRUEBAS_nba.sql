﻿USE nba;

SELECT * FROM equipos;
SELECT * FROM estadisticas;
SELECT * FROM jugadores;
SELECT * FROM partidos;



/* (11) ¿Cuántos partidos hay registrados? */
SELECT COUNT(*) FROM partidos;

/* (12) ¿Cuál es el promedio de puntos por partido? */
SELECT AVG(puntos_local)+AVG(puntos_visitante) FROM partidos;

/* (5) ¿Cuánto mide el jugador más alto de la NBA? */
SELECT Nombre FROM jugadores WHERE altura=(SELECT MAX(Altura) FROM jugadores);
SELECT SUBSTRING_INDEX(altura,'-',1)*0.3048+SUBSTRING_INDEX(altura,'-',-1)*0.0254 FROM jugadores;

/* (4) Jugador más delgado de la NBA */
SELECT Nombre FROM jugadores WHERE peso=(SELECT MIN(Peso) FROM jugadores);

/* (2) Nombre y temporada del jugador con más puntos por partido */
SELECT * FROM estadisticas;
SELECT MAX(Puntos_por_partido) FROM estadisticas;

SELECT Nombre,temporada FROM 
(SELECT 
  jugador,temporada 
FROM 
  estadisticas
 WHERE Puntos_por_partido=(SELECT MAX(Puntos_por_partido) FROM estadisticas)) c1 JOIN jugadores j ON c1.jugador=j.codigo;


/* (3) Temporada con estadísticas de más jugadores */
SELECT temporada WHERE SELECT MAX(n) FROM (SELECT temporada,COUNT(*) n FROM estadisticas GROUP BY temporada) c1;
SELECT temporada,COUNT(*) FROM estadisticas GROUP BY temporada;
SELECT * FROM estadisticas;

/* (7) Nombre del equipo que más punto ha anotado en casa */
SELECT DISTINCT equipo_local FROM partidos WHERE puntos_local=(SELECT MAX(puntos_local) FROM partidos);
SELECT * FROM partidos;

/* (10) ¿En cuántos equipos ha jugado Gasol? */
SELECT Nombre_equipo FROM jugadores WHERE Nombre='Pau Gasol';
SELECT * FROM jugadores;

/* (9) ¿En qué temporada ha anotado más puntos Gasol? */
SELECT codigo FROM jugadores WHERE Nombre='Pau Gasol';
SELECT * FROM estadisticas WHERE jugador=(SELECT codigo FROM jugadores WHERE Nombre='Pau Gasol');

/* (8) ¿En qué ciudad hay más equipos de basket? */
SELECT ciudad,COUNT(*) FROM equipos GROUP BY Ciudad;
SELECT * FROM equipos;