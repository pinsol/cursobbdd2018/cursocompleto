﻿USE hospitales;

/* (7) A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento
  cuyo oficio sea EMPLEADO (utilizar GROUP BY para agrupar por departamento.
  En la cláusula WHERE habrá que indicar que el oficio es EMPLEADO) */
SELECT
 dept_no,COUNT(*) n 
FROM
 emple 
WHERE
 oficio='EMPLEADO' 
GROUP BY dept_no
;

/* (18) Realizar una consulta en la que se muestre por cada código hospital
  el nombre de las especialidades que tiene.
  Ordenar por código de hospital y especialidad */
SELECT
 cod_hospital, especialidad 
FROM
 medicos 
 ORDER BY cod_hospital,especialidad
;


/* (15) Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades */
SELECT
 estanteria,SUM(unidades) 
FROM
  herramientas 
GROUP BY estanteria
;

/* (19) Realizar una consulta en la que aparezca por cada hospital
  y en cada especialidad el número de médicos
  (tendrás que partir de la consulta anterior y utilizar GROUP BY) */
SELECT
 cod_hospital,especialidad,COUNT(*) n 
FROM
 medicos 
GROUP BY
 cod_hospital,especialidad
;


USE emple_depart;

/* (4) Visualizar el nombre de los empleados vendedores del departamento VENTAS
  (Nombre del departamento=VENTAS, oficio=VENDEDOR) */
SELECT
 e.apellido 
FROM
 emple e JOIN depart d USING(dept_no) 
WHERE
 d.dnombre='VENTAS' AND e.oficio='VENDEDOR'
;

/* (1) Visualizar el número de empleados de cada departamento.
  Utilizar GROUP BY para agrupar por departamento */
SELECT
 d.dept_no,COUNT(*) 
FROM
 emple e JOIN depart d USING(dept_no)
GROUP BY d.dept_no
;

/* (6) Visualizar los oficios de los empleados del departamento VENTAS */
SELECT
 DISTINCT e.oficio 
FROM
 emple e JOIN depart d USING(dept_no) 
WHERE
 d.dnombre='VENTAS'
;

/* (5) Visualizar el número de vendedores del departamento VENTAS
  (utilizar la función COUNT sobre la consulta (4)) */
SELECT
 COUNT(*)
FROM
 emple e JOIN depart d USING(dept_no) 
WHERE
 d.dnombre='VENTAS' AND e.oficio='VENDEDOR'
;

/* (10) Para cada oficio obtener la suma de salarios */
SELECT
 oficio,SUM(salario) 
FROM
 emple 
GROUP BY oficio
;

/* (3) Hallar la media de los salarios de cada código de departamento con empleados
  (utilizar la función AVG y GROUP BY) */
SELECT
 dept_no,AVG(salario) 
FROM emple 
GROUP BY dept_no
;

/* (13) Mostrar el número de oficios distintos de cada departamento */
SELECT
 dept_no,COUNT(DISTINCT oficio) 
FROM
 emple 
GROUP BY dept_no
;