﻿CREATE DATABASE ejemplo3yii;
USE ejemplo3yii;

CREATE OR REPLACE TABLE tenistas(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  correo varchar(20),
  activo bool,
  fechaBaja date,
  fechaNacimiento date,
  altura int,
  peso int,
  idnacion int,
  PRIMARY KEY(id)
  );

 ALTER TABLE tenistas ADD COLUMN IF NOT EXISTS edad int;

CREATE OR REPLACE TABLE naciones(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  continente varchar(20),
  victorias int,
  PRIMARY KEY(id)
  );

ALTER TABLE tenistas ADD CONSTRAINT FKtenistaNacion FOREIGN KEY(idnacion)
  REFERENCES naciones(id) ON DELETE CASCADE ON UPDATE CASCADE;
