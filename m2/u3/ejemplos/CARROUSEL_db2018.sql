﻿CREATE DATABASE db2018;

USE db2018;

CREATE TABLE datos(
  id_dato int AUTO_INCREMENT,
  datos varchar(127),
  PRIMARY KEY(id_dato)
  );

SELECT * FROM datos;

INSERT INTO datos (datos)
  VALUES (now());

ALTER TABLE datos ADD ip varchar(15);

SELECT * FROM datos;

SELECT COUNT(DISTINCT ip) FROM datos;

CREATE TABLE usuarios(
  id_usuario int AUTO_INCREMENT,
  ip varchar(15),
  nombre varchar(31),
  PRIMARY KEY(id_usuario)
  );

SELECT * FROM usuarios;

ALTER TABLE datos ADD INDEX(ip);
SELECT * FROM usuarios;

SELECT ip,COUNT(*) n FROM datos GROUP BY 1;

SELECT MAX(n) FROM (
SELECT ip,COUNT(*) n FROM datos GROUP BY 1) c1;

SELECT * FROM (
    SELECT ip,COUNT(*) n FROM datos
      WHERE ip IS NOT NULL
      AND ip<>'172.31.128.159'
      GROUP BY 1
     HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
           SELECT ip,COUNT(*) n FROM datos
            WHERE ip IS NOT NULL
            AND ip<>'172.31.128.159'
          GROUP BY 1
        ) c1
    )  
  ) c2 JOIN usuarios USING(ip);

SELECT * FROM (
    SELECT ip,COUNT(*) n FROM datos
      WHERE ip IS NOT NULL
      AND ip NOT IN (
        SELECT ip FROM usuarios
          WHERE nombre='Yo'
      )
      GROUP BY 1
     HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
           SELECT ip,COUNT(*) n FROM datos
            WHERE ip IS NOT NULL
            AND ip NOT IN (
              SELECT ip FROM usuarios
                WHERE nombre='Yo'
            )
          GROUP BY 1
        ) c1
    )  
  ) c2 JOIN usuarios USING(ip);

SELECT nombre,COUNT(ip) FROM datos JOIN usuarios USING(ip) GROUP BY ip;
SELECT * FROM datos;
SELECT * FROM usuarios;

SELECT * FROM datos JOIN usuarios USING(ip);

ALTER TABLE usuarios ADD email varchar(127);

UPDATE usuarios
  SET email='dblanco@alpeformacion.es'
  WHERE ip='172.31.128.235';

SELECT * FROM datos;

ALTER TABLE datos CHANGE datos datos
  datetime;

SELECT * FROM usuarios WHERE
  ip NOT IN (
    SELECT DISTINCT ip FROM datos WHERE datos>NOW()-INTERVAL 3 MINUTE
);
