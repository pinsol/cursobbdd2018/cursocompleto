-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-03-2019 a las 11:59:47
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `m2u2practica1_atletismo`
--
CREATE DATABASE IF NOT EXISTS `m2u2practica1_atletismo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `m2u2practica1_atletismo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportista`
--

DROP TABLE IF EXISTS `deportista`;
CREATE TABLE IF NOT EXISTS `deportista` (
  `codDep` int(11) NOT NULL AUTO_INCREMENT,
  `nomapDep` varchar(50) DEFAULT NULL,
  `provinciaDep` varchar(20) DEFAULT NULL,
  `fechanacDep` date DEFAULT NULL,
  `dniDep` varchar(10) DEFAULT NULL,
  `domicilioDep` varchar(20) DEFAULT NULL,
  `codposDep` int(11) DEFAULT NULL,
  `telefonoDep` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`codDep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

DROP TABLE IF EXISTS `prueba`;
CREATE TABLE IF NOT EXISTS `prueba` (
  `codReu` int(11) DEFAULT NULL,
  `numPrueba` int(11) NOT NULL AUTO_INCREMENT,
  `codTipo` int(11) DEFAULT NULL,
  `horaPrueba` time DEFAULT NULL,
  `lugarPrueba` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`numPrueba`),
  UNIQUE KEY `codReu` (`codReu`),
  KEY `FK_prueba_tipoPrueba_codTipo` (`codTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultado`
--

DROP TABLE IF EXISTS `resultado`;
CREATE TABLE IF NOT EXISTS `resultado` (
  `codReu` int(11) DEFAULT NULL,
  `numPrueba` int(11) DEFAULT NULL,
  `inscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `codDep` int(11) DEFAULT NULL,
  `marcaDep` float DEFAULT NULL,
  `posicionDep` int(11) DEFAULT NULL,
  PRIMARY KEY (`inscripcion`),
  UNIQUE KEY `codReu` (`codReu`),
  UNIQUE KEY `numPrueba` (`numPrueba`),
  KEY `FK_resultado_deportista_codDep` (`codDep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reunion`
--

DROP TABLE IF EXISTS `reunion`;
CREATE TABLE IF NOT EXISTS `reunion` (
  `codReu` int(11) NOT NULL AUTO_INCREMENT,
  `fechaReu` date DEFAULT NULL,
  `lugarReu` varchar(50) DEFAULT NULL,
  `nombreReu` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codReu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoprueba`
--

DROP TABLE IF EXISTS `tipoprueba`;
CREATE TABLE IF NOT EXISTS `tipoprueba` (
  `codTipo` int(11) NOT NULL AUTO_INCREMENT,
  `destTipo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `prueba`
--
ALTER TABLE `prueba`
  ADD CONSTRAINT `FK_prueba_reunion_codReu` FOREIGN KEY (`codReu`) REFERENCES `reunion` (`codReu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prueba_tipoPrueba_codTipo` FOREIGN KEY (`codTipo`) REFERENCES `tipoprueba` (`codTipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `resultado`
--
ALTER TABLE `resultado`
  ADD CONSTRAINT `FK_resultado_deportista_codDep` FOREIGN KEY (`codDep`) REFERENCES `deportista` (`codDep`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_resultado_prueba_numPrueba` FOREIGN KEY (`numPrueba`) REFERENCES `prueba` (`numPrueba`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
