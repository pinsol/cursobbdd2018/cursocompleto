﻿                                /* m2u2 - Practica 1 */ 
                                   /* ATLETISMO */

DROP DATABASE IF EXISTS m2u2practica1_atletismo;
CREATE DATABASE m2u2practica1_atletismo;
USE m2u2practica1_atletismo;

CREATE OR REPLACE TABLE deportista(
  codDep int AUTO_INCREMENT PRIMARY KEY,
  nomapDep varchar(50),
  provinciaDep varchar(20),
  fechanacDep date,
  dniDep varchar(10),
  domicilioDep varchar(20),
  codposDep int,
  telefonoDep varchar(12)
  );

CREATE OR REPLACE TABLE tipoPrueba(
  codTipo int AUTO_INCREMENT PRIMARY KEY,
  destTipo varchar(20)
  );

CREATE OR REPLACE TABLE reunion(
  codReu int AUTO_INCREMENT PRIMARY KEY,
  fechaReu date,
  lugarReu varchar(50),
  nombreReu varchar(20)
);

CREATE OR REPLACE TABLE prueba(
  codReu int UNIQUE KEY,
  numPrueba int AUTO_INCREMENT PRIMARY KEY,
  codTipo int,
  horaPrueba time,
  lugarPrueba varchar(20)
  );

CREATE OR REPLACE TABLE resultado(
  codReu int UNIQUE KEY,
  numPrueba int UNIQUE KEY,
  inscripcion int AUTO_INCREMENT PRIMARY KEY,
  codDep int,
  marcaDep float,
  posicionDep int
  );

ALTER TABLE prueba
ADD CONSTRAINT FK_prueba_tipoPrueba_codTipo FOREIGN KEY (codTipo)
REFERENCES tipoPrueba (codTipo) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE prueba
ADD CONSTRAINT FK_prueba_reunion_codReu FOREIGN KEY (codReu)
REFERENCES reunion (codReu) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE resultado
ADD CONSTRAINT FK_resultado_prueba_numPrueba FOREIGN KEY (numPrueba)
REFERENCES prueba (numPrueba) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE resultado
ADD CONSTRAINT FK_resultado_deportista_codDep FOREIGN KEY (codDep)
REFERENCES deportista (codDep) ON DELETE CASCADE ON UPDATE CASCADE;