-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2019 a las 13:33:27
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `m2u2practica1_hotel`
--
CREATE DATABASE IF NOT EXISTS `m2u2practica1_hotel` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `m2u2practica1_hotel`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `DNI` varchar(10) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidos` varchar(20) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `direccion` varchar(20) DEFAULT NULL,
  `fechaNac` date DEFAULT NULL,
  `poblacion` varchar(20) DEFAULT NULL,
  `codPostal` int(11) DEFAULT NULL,
  `Provincia` varchar(20) DEFAULT NULL,
  `Pais` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE IF NOT EXISTS `gastos` (
  `codReserva` int(11) NOT NULL,
  `idGasto` int(11) NOT NULL AUTO_INCREMENT,
  `descGasto` float DEFAULT NULL,
  `importeGasto` float DEFAULT NULL,
  PRIMARY KEY (`codReserva`),
  UNIQUE KEY `idGasto` (`idGasto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

DROP TABLE IF EXISTS `habitacion`;
CREATE TABLE IF NOT EXISTS `habitacion` (
  `numHabitacion` int(11) NOT NULL,
  `idTipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`numHabitacion`),
  KEY `FK_habitacion_tipoHabitacion_idTipo` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `codReserva` int(11) NOT NULL AUTO_INCREMENT,
  `fechaEntrada` date DEFAULT NULL,
  `fechaSalida` date DEFAULT NULL,
  `numHabitacion` int(11) DEFAULT NULL,
  `DNI` varchar(10) DEFAULT NULL,
  `IVA` float DEFAULT NULL,
  PRIMARY KEY (`codReserva`),
  KEY `FK_reserva_clientes_DNI` (`DNI`),
  KEY `FK_reserva_habitacion_numHabitacion` (`numHabitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipohabitacion`
--

DROP TABLE IF EXISTS `tipohabitacion`;
CREATE TABLE IF NOT EXISTS `tipohabitacion` (
  `idTipo` int(11) NOT NULL,
  `categoria` varchar(20) DEFAULT NULL,
  `descripcion` blob,
  `precioHab` float DEFAULT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD CONSTRAINT `FK_habitacion_tipoHabitacion_idTipo` FOREIGN KEY (`idTipo`) REFERENCES `tipohabitacion` (`idTipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `FK_reserva_clientes_DNI` FOREIGN KEY (`DNI`) REFERENCES `clientes` (`DNI`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_reserva_gastos_codReserva` FOREIGN KEY (`codReserva`) REFERENCES `gastos` (`codReserva`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_reserva_habitacion_numHabitacion` FOREIGN KEY (`numHabitacion`) REFERENCES `habitacion` (`numHabitacion`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
