﻿                                /* m2u2 - Practica 3 */ 
                             /* DELEGACION - TRABAJADORES */

DROP DATABASE IF EXISTS practica3yii;
CREATE DATABASE practica3yii;
USE practica3yii;

CREATE OR REPLACE TABLE trabajadores(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(20),
  apellidos varchar(20),
  fechaNacimiento date,
  foto varchar(30),
  delegacion int
  );

CREATE OR REPLACE TABLE delegacion(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(20),
  poblacion varchar(20),
  direccion varchar(30)
  );

ALTER TABLE trabajadores
ADD CONSTRAINT FK_trabajadores_delegacion FOREIGN KEY (delegacion)
REFERENCES delegacion (id) ON DELETE CASCADE ON UPDATE CASCADE;