﻿USE provincias;

EXPLAIN provincias;

EXPLAIN     -- antes de la consulta
SELECT provincia FROM provincias WHERE poblacion=
  (SELECT MAX(poblacion) FROM provincias);

EXPLAIN
SELECT provincia FROM provincias
  ORDER BY poblacion DESC LIMIT 1;

ALTER TABLE provincias ADD INDEX(poblacion);  -- añadimos Indice

SELECT BENCHMARK(2,SLEEP(2));  -- para comprobar lo q tarda una fx

CREATE FUNCTION madrid()
  RETURNS varchar(31)
  BEGIN
    RETURN (  -- se puede poner antes del END si se pone la fx entre parentesis
      SELECT provincia FROM provincias WHERE poblacion=
        (SELECT MAX(poblacion) FROM provincias)
    );
  END;

SELECT madrid();

SELECT BENCHMARK(10e4,madrid());

CREATE FUNCTION madrid2()
  RETURNS varchar(31)
  BEGIN
  RETURN(
    SELECT provincia FROM provincias
  ORDER BY poblacion DESC LIMIT 1
    ); 
  END;

SELECT madrid2();

SELECT BENCHMARK(10e4,madrid2());

/* Al añadir el index es mas rapida madrid() que madrid2()
  xo aun asi, quitando el index, sigue siendo mas rapida
  no es el resultado esperado
  */

  SELECT * FROM information_schema.tables;
  SELECT * FROM information_schema.TABLES
     WHERE TABLE_SCHEMA='nba';

  
 USE nba;
SELECT * FROM partidos;
EXPLAIN partidos;

/* Partido en el que se ha marcado menos puntos en la temporada 06/07 */
SELECT * FROM partidos;

EXPLAIN
SELECT codigo FROM partidos WHERE puntos_local=(
 SELECT MIN(puntos_local) FROM partidos 
) AND puntos_visitante=(
 SELECT MIN(puntos_visitante) FROM partidos
) AND temporada='06/07';

EXPLAIN
SELECT codigo FROM partidos WHERE temporada='06/07'
  ORDER BY puntos_local,puntos_visitante LIMIT 1;


CREATE FUNCTION nba()
  RETURNS int
  BEGIN
    RETURN(
SELECT codigo FROM partidos WHERE puntos_local=(
 SELECT MIN(puntos_local) FROM partidos 
) AND puntos_visitante=(
 SELECT MIN(puntos_visitante) FROM partidos
) AND temporada='06/07'
);
  END;

SELECT nba();

CREATE FUNCTION nba2()
  RETURNS int
  BEGIN
  RETURN(
    SELECT codigo FROM partidos WHERE temporada='06/07'
  ORDER BY puntos_local,puntos_visitante LIMIT 1
    ); 
  END;

  SELECT nba2();


 SELECT BENCHMARK(100,nba());
 SELECT BENCHMARK(100,nba2());


/* cual es el mayor numero de puntos locales marcados */

SELECT MAX(puntos_local) FROM partidos;

SELECT puntos_local FROM partidos ORDER BY puntos_local DESC LIMIT 1;


CREATE FUNCTION nba3()
RETURNS integer
  BEGIN
  RETURN(
   SELECT MAX(puntos_local) FROM partidos
  ); 
  END;

SELECT nba3();

CREATE FUNCTION nba4()
RETURNS int
  BEGIN
    RETURN(
      SELECT puntos_local FROM partidos ORDER BY puntos_local DESC LIMIT 1
      ); 
  END;

SELECT nba4();

SELECT BENCHMARK(1e3,nba3());
SELECT BENCHMARK(1e3,nba4());

/* cuantos partidos jugados por los knicks (equipo local) en nueva york */

SELECT COUNT(*) FROM partidos p
  JOIN equipos e ON p.equipo_local = e.Nombre WHERE e.Ciudad='new york';

SELECT COUNT(*) FROM partidos p
  JOIN (
    SELECT nombre FROM equipos WHERE Ciudad='New york') c1 ON c1.Nombre=p.equipo_local
  JOIN equipos e ON p.equipo_local = e.Nombre;

CREATE FUNCTION nba5()
RETURNS integer
  BEGIN
  RETURN(
   SELECT COUNT(*) FROM partidos p
  JOIN equipos e ON p.equipo_local = e.Nombre WHERE e.Ciudad='new york'
  ); 
  END;

SELECT nba5();

CREATE FUNCTION nba6()
RETURNS int
  BEGIN
    RETURN(
      SELECT COUNT(*) FROM partidos p
  JOIN (
    SELECT nombre FROM equipos WHERE Ciudad='New york') c1 ON c1.Nombre=p.equipo_local
  JOIN equipos e ON p.equipo_local = e.Nombre
      ); 
  END;

SELECT nba6();

SELECT BENCHMARK(1e4,nba5());
SELECT BENCHMARK(1e4,nba6());