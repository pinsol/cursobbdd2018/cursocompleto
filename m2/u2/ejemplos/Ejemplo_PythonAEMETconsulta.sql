﻿CREATE DATABASE db2018_python;
USE db2018_python;

CREATE TABLE python(
  id int AUTO_INCREMENT PRIMARY KEY,
  instante datetime
  );
INSERT INTO python (instante)
  VALUES (now());
SELECT * FROM python p;

CREATE TABLE aemet(
  id int AUTO_INCREMENT PRIMARY KEY,
  fecha date,
  lluvia int,
  inserccion datetime,
  UNIQUE(fecha)
);

ALTER TABLE python ADD edad int;
ALTER TABLE python ADD nacimiento int;

SELECT * FROM aemet a;
INSERT INTO aemet (fecha, lluvia, inserccion)
  VALUES ('','', NOW());

SELECT * FROM aemet a;
SELECT fecha FROM aemet WHERE lluvia=(SELECT MIN(lluvia) FROM aemet);

ALTER TABLE aemet ADD COLUMN f_prediccion date;
SELECT * FROM aemet a;
UPDATE aemet a SET f_prediccion=date(inserccion);

ALTER TABLE aemet DROP INDEX (fecha);
ALTER TABLE aemet ADD INDEX (fecha,f_prediccion);

SELECT fecha,lluvia FROM aemet WHERE lluvia=(SELECT MIN(lluvia) FROM aemet WHERE date(inserccion)=(
SELECT MAX(date(inserccion)) FROM aemet))AND date(inserccion)=(SELECT MAX(date(inserccion)) FROM aemet);